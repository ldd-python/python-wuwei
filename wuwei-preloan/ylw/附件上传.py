# -*- coding: UTF-8 -*-
import json
import os
import types
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService, AssetService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'


class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class SealCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(SealCall, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(SealCall, self).__eval_business_result(result)
        print "__eval_business_result  rpc@cfca_seal_call %s" % self.rpc_channel_id
        pass


class AssetService(AssetService):
    def __init__(self):
        pass

    def update_contract_status(self, contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                               contract_external_no, remark):
        return self.updateContractStatusAndRemark(contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                                                  contract_external_no, remark)

    def get_contract(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def update_file_status(self, file_uuid, file_status, sealed_file_address):
        return self.updateFileStatus(file_uuid, file_status, sealed_file_address)


class CreditService(CreditService):
    def __init__(self):
        pass

    def get_credit_application_by_merchant_creditNo(self, product_code, veda_credit_no):
        credit_json = self.getCreditApplicationByMerchantCreditNo(product_code, veda_credit_no)
        credit = json.loads(credit_json)
        return credit

    def get_credit_application_by_veda_creditNo(self, product_code, merchant_credit_no):
        credit_json = self.getCreditApplicationByVedaCreditNo(product_code, merchant_credit_no)
        credit = json.loads(credit_json)
        return credit


class RpcCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(RpcCall, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(RpcCall, self).__eval_business_result(result)
        print "__eval_business_result  rpc@file_upload_call %s" % self.rpc_channel_id
        pass


def check_file_type(application_data):
    file_type_list = []
    for file_info in application_data:
        file_type = file_info['fileType']
        file_type_list.append(file_type)
    if ('3' in file_type_list) or ('5' in file_type_list):
        return True
    return False


def update_seal_status(file_uuid, seal_status, sealed_file_address):
    asset_service = AssetService()
    asset_service.update_file_status(file_uuid, seal_status, sealed_file_address)


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    file_info = json.loads(request_content)
    return file_info


def file_validate_data(file_info):
    validate_result = True

    credit_service = CreditService()
    asset_service = AssetService()

    if not (file_info.has_key('merchantCreditNo') and file_info['merchantCreditNo'] != ""):
        validate_result = False
        return validate_result

    credit = credit_service.get_credit_application_by_veda_creditNo(file_info['productCode'],
                                                                    file_info['merchantCreditNo'])

    print("credit is %s" % credit)
    if credit is None:
        validate_result = False
        return validate_result

    if not (file_info.has_key('merchantCreditNo') and file_info['merchantCreditNo'] != ""):
        validate_result = False
        return validate_result

    if (file_info.has_key('merchantContractNo') and file_info['merchantContractNo'] != ""):
        # validate_result = False
        # return validate_result
        contract = asset_service.get_contract(file_info['merchantContractNo'])
        print("contract is %s " % contract)
        if contract is None:
            validate_result = False
            return validate_result

    if file_info['fileType'] != '3' and file_info['fileType'] != '5':
        validate_result = False
        return validate_result

    return validate_result


def make_dir(file_path):
    is_exists = os.path.exists(file_path)
    if not is_exists:
        os.makedirs(file_path)
    return file_path


def extract_call_cfca_data(application_data):
    call_cfca_data = {}

    call_cfca_data['contractFilePath'] = application_data['fileAddress']

    file_path = application_data['fileAddress']
    file_name = os.path.basename(file_path)
    file_name_after_seal = file_name.split('.')[0] + '_sealed.' + file_name.split('.')[1]

    call_cfca_data['contractFilePathAfterStamped'] = os.path.dirname(file_path) + '/' + file_name_after_seal

    return call_cfca_data


def extract_call_tianwei_create_contract_data(application_data):
    call_tianwei_data = {}
    call_tianwei_data['contractFilePath'] = application_data['fileAddress']
    call_tianwei_data['merchantContractNo'] = application_data['fileUuid']

    return call_tianwei_data


def extract_call_tianwei_seal_data(application_data, tianwei_create_contract_rpc_response):
    call_tianwei_data = {}

    print ("天威创建合同返回内容%s" % tianwei_create_contract_rpc_response)
    tianwei_response = json.loads(tianwei_create_contract_rpc_response)

    call_tianwei_data['twContractId'] = tianwei_response['twContractId']

    file_path = application_data['fileAddress']
    file_name = os.path.basename(file_path)
    file_name_after_seal = file_name.split('.')[0] + '_sealed.' + file_name.split('.')[1]

    call_tianwei_data['sealedContractSavePath'] = os.path.dirname(file_path) + '/' + file_name_after_seal

    return call_tianwei_data


def build_file_upload_data(file_path, product_code):
    file_upload_data = {}
    print "-------------------------------------"
    file_upload_data['fileType'] = "sftp"
    today = datetime.datetime.now().strftime('%Y%m%d')
    file_upload_data['uploadPath'] = "/download/" + product_code + "/" + today + "/Assetattachment"
    file_name = file_path.split("/")[-1]
    file_upload_data['uploadFileName'] = file_name
    print("file_name是%s" % file_name)
    file_upload_data['localPath'] = file_path
    return file_upload_data


def extract_call_ssq_data(application_data):
    call_fadada_data = {}
    call_fadada_data['contractFilePath'] = application_data['fileAddress']
    call_fadada_data['fileUuid'] = application_data['fileUuid']
    return call_fadada_data


def run_script(request_data):
    application_data = extract_business_data(request_data)
    print("application的值是%s" % application_data)
    schedule_plan_uuid = request_data['schedulePlanUuid']

    # 遍历附件
    for file_info in application_data:
        print("打印出fileinfo的值是%s" % file_info)
        validate_result = file_validate_data(file_info)
        print("validate_result is %s " % validate_result)
        if not validate_result:
            continue

        schedule_plan_uuid = request_data['schedulePlanUuid']

        call_ssq_data = extract_call_ssq_data(file_info)
        fadada_call_obj = RpcCall("YOULIWANG_FADADA_VERIFY", schedule_plan_uuid, call_ssq_data)

        yield fadada_call_obj.call()
        print ("fadada_call_obj.response().rpc_business_state is ", fadada_call_obj.response().rpc_business_state)

        if fadada_call_obj.response().rpc_business_state == RESPONSE_BUSINESS_FAILED:
            update_seal_status(file_info['fileUuid'], 4, None)
            continue

        if fadada_call_obj.response().rpc_business_state == RESPONSE_BUSINESS_OK:
            update_seal_status(file_info['fileUuid'], 3, None)

        # 如果不是预签章合同则跳过
        if file_info['fileType'] != '3':
            print ('授权书跳过盖章')
            continue

        print ('预签章合同盖章开始')
        # 天威
        call_tianwei_create_contract_data = extract_call_tianwei_create_contract_data(file_info)
        tianwei_seal_create_call_req = RpcCall("YOULIWANG_TIANWEI_CREATE_CONTRACT", schedule_plan_uuid,
                                               call_tianwei_create_contract_data)
        yield tianwei_seal_create_call_req.call()

        if tianwei_seal_create_call_req.response().rpc_business_state == RESPONSE_BUSINESS_FAILED:
            update_seal_status(file_info['fileUuid'], 6, None)
            continue

        call_tianwei_seal_data = extract_call_tianwei_seal_data(file_info,
                                                                tianwei_seal_create_call_req.response().rpc_result)
        tianwei_seal_call_req = RpcCall("YOULIWANG_TIANWEI_SIGN_CONTRACT", schedule_plan_uuid, call_tianwei_seal_data)
        yield tianwei_seal_call_req.call()

        if tianwei_seal_call_req.response().rpc_business_state == RESPONSE_BUSINESS_FAILED:
            update_seal_status(file_info['fileUuid'], 6, None)
            continue

        if tianwei_seal_call_req.response().rpc_business_state == RESPONSE_BUSINESS_OK:
            sealed_contract_save_path = call_tianwei_seal_data['sealedContractSavePath']
            update_seal_status(file_info['fileUuid'], 5, sealed_contract_save_path)
            print "开始进行文件上传"
            file_upload_data = build_file_upload_data(sealed_contract_save_path, file_info['productCode'])
            print("文件上传参数是%s" % file_upload_data)
            upload_file = RpcCall("YOULIWANG_FILE_UPLOAD", schedule_plan_uuid, file_upload_data)
            upload_file.emit()


def create_engin(request_data):
    application_data = extract_business_data(request_data)
    check_result = check_file_type(application_data)
    print("check_result is %s " % check_result)
    if check_result:
        runtime = run_script(request_data)
        engine = AsyncEngine(runtime)
        result = engine.one_step()
        if "end" != result:
            return engine
        else:
            return result
    else:
        return "end"
