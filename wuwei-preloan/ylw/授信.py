# -*- coding = UTF-8 -*-
import json
import types
import re
import datetime
import time

from com.suidifu.preloan.scheduler.service import CreditService, SummaryService, AssetService
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'


class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class CreditService(CreditService):
    def __init__(self):
        pass

    def get_credit_application(self, credit_application_uuid):
        return self.getCreditApplicationByCreditApplicationUuid(credit_application_uuid)

    def create_borrower(self, application_data):
        return self.createBorrower(application_data['certificateType'], application_data['certificateNo'],
                                   application_data['name'])

    def update_credit_status(self, credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                             external_customer_no_section, remark):
        return self.updateCreditStatusNotEncode(credit_application_uuid, credit_status_ordinal, credit_line,
                                                borrower_uuid,
                                                external_customer_no_section, remark)

    def get_success_credit_line(self, product_code, certificate_no, status):
        return json.loads(self.getSuccessCreditLine(product_code, certificate_no, status))


class SummaryService(SummaryService):
    def __init__(self):
        pass

    def get_summary_by_uuid(self, summary_uuid):
        summery_vo = self.getSummaryAsset(summary_uuid)
        return json.loads(summery_vo)

    def update_summary_status(self, summary_asset_uuid, summary_status, remark):
        return self.updateSummaryAssetStatusNotEncode(summary_asset_uuid, summary_status, remark)


class AssetService(AssetService):
    def __init__(self):
        pass

    def create_loan_contract(self, summary_uuid, veda_credit_no):
        result = self.createLoanContractApplicationFromCredit(summary_uuid, veda_credit_no)
        return json.loads(result)


# 创建借款用户
def create_borrower(application_data):
    credit_service = CreditService()
    return credit_service.create_borrower(application_data)


# 更新授信状态
def update_credit_status(credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                         external_customer_no_section, remark):
    credit_service = CreditService()
    credit_service.update_credit_status(credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                                        external_customer_no_section, remark)


class RpcCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        super(RpcCall, self).__init__(gateway_channel_id, schedule_plan_uuid, params)
        pass

    def __eval_business_result(self, result):
        super(RpcCall, self).__eval_business_result(result)
        print("__eval_business_result  rpc@CreditCallback %s" % self.rpc_channel_id)
        pass

def check_birthDate_is_match(certificate_no,birthDate):
    print "certificate_no:{}",certificate_no
    print "birthDate:{}",birthDate
    field_validate_result = {}
    if len(certificate_no) != 18 and len(certificate_no) != 15:
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，证件号码位数不正确'
        return field_validate_result

    if len(certificate_no) == 18:
        birthDay = certificate_no[6:14]
        s = time.mktime(time.strptime(birthDay,"%Y%m%d"))
        if birthDate != s*1000:
            field_validate_result['status'] = False
            field_validate_result['message'] = '授信失败，出生日期与证件号日期不匹配'
            return field_validate_result

    if len(certificate_no) == 15:
        birthDay = '19' + certificate_no[6:12]
        s = time.mktime(time.strptime(birthDay,"%Y%m%d"))
        if birthDate != s*1000:
            field_validate_result['status'] = False
            field_validate_result['message'] = '授信失败，出生日期与证件号日期不匹配'
            return field_validate_result

    field_validate_result['status'] = True
    field_validate_result['message'] = ''
    return field_validate_result

# 获取credit_application
def extract_application_data(request_data):
    request_content = request_data['requestContent']
    application = json.loads(request_content)
    return application


# 字段校验方法
def field_validate_data(application_data):
    field_validate_result = {}

    if (not application_data.has_key('name')) or (not application_data['name']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，姓名不能为空'
        return field_validate_result

    if (not application_data.has_key('certificateType')) or (not application_data['certificateType']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，证件类型不能为空'
        return field_validate_result

    if (not application_data.has_key('mobile')) or (not application_data['mobile']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，手机号不能为空'
        return field_validate_result

    if (not application_data.has_key('certificateNo')) or (not application_data['certificateNo']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，证件号码不能为空'
        return field_validate_result

    if (not application_data.has_key('birthDate')) or (not application_data['birthDate']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，出生日期不能为空'
        return field_validate_result

    if (not application_data.has_key('applyAmount')) or (not application_data['applyAmount']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，授信申请金额不能为空'
        return field_validate_result

    if (not application_data.has_key('zoneBelong')) or (not application_data['zoneBelong']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，所属地区不能为空'
        return field_validate_result

    if (not application_data.has_key('countryBelong')) or (not application_data['countryBelong']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，所属国别不能为空'
        return field_validate_result

    # 证件类型是身份证号，对证件号进行正则校验
    if application_data['certificateType'] == 'ID_CARD':
        regex = '(^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}[0-9Xx]$)'
        certificate_no = application_data['certificateNo']
        if not re.match(regex, certificate_no):
            field_validate_result['status'] = False
            field_validate_result['message'] = '授信失败，证件号格式不正确'
            return field_validate_result
        # birthDate与证件号生日是否匹配
        field_validate_result = check_birthDate_is_match(application_data['certificateNo'],
                                                   application_data['birthDate'])
        if field_validate_result['status']:
            pass
        else:
            return field_validate_result

    # 校验年龄
    age = get_age_by_idcard_num(application_data['certificateNo'])
    # user_age < '22' or user_age > '55'
    if age < 22 or age > 55:
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，年龄需在22~55周岁之间'
        return field_validate_result

    borrower_info = application_data['borrowerApplicationInfo']

    if (not borrower_info.has_key('highestDegree')) or (not borrower_info['highestDegree']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，最高学位不能为空'
        return field_validate_result

    if (not borrower_info.has_key('highestDiploma')) or (not borrower_info['highestDiploma']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，最高学历不能为空'
        return field_validate_result

    if (not borrower_info.has_key('jobCategory')) or (not borrower_info['jobCategory']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，职业类别不能为空'
        return field_validate_result

    if (not borrower_info.has_key('jobTitle')) or (not borrower_info['jobTitle']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，职称不能为空'
        return field_validate_result

    if (not borrower_info.has_key('job')) or (not borrower_info['job']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，职务不能为空'
        return field_validate_result

    if (not borrower_info.has_key('organizationIndustry')) or (not borrower_info['organizationIndustry']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，单位所属行业不能为空'
        return field_validate_result

    if (not borrower_info.has_key('organizationAddress')) or (not borrower_info['organizationAddress']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，单位地址不能为空'
        return field_validate_result

    if (not borrower_info.has_key('maritalStatus')) or (not borrower_info['maritalStatus']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，婚姻状况不能为空'
        return field_validate_result

    if (not borrower_info.has_key('residenceProvince')) or (not borrower_info['residenceProvince']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，居住地所在省不能为空'
        return field_validate_result

    if (not borrower_info.has_key('residenceCity')) or (not borrower_info['residenceCity']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，居住地所在市不能为空'
        return field_validate_result

    if (not borrower_info.has_key('residenceAddress')) or (not borrower_info['residenceAddress']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，居住地址不能为空'
        return field_validate_result

    # if (not borrower_info.has_key('residencePostalCode')) or (not borrower_info['residencePostalCode']):
    #     field_validate_result['status'] = False
    #     field_validate_result['message'] = '授信失败，居住地址邮编不能为空'
    #     return field_validate_result

    if (not borrower_info.has_key('residenceCondition')) or (not borrower_info['residenceCondition']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，居住状况不能为空'
        return field_validate_result

    if (not borrower_info.has_key('country')) or (not borrower_info['country']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，居住国家不能为空'
        return field_validate_result

    bank_info_json = borrower_info['bankcardInfoSection']
    bank_info = json.loads(bank_info_json)
    if (not bank_info.has_key('bankcardNo')) or (not bank_info['bankcardNo']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，还款银行卡账号为空'
        return field_validate_result

    if (not bank_info.has_key('bankName')) or (not bank_info['bankName']):
        field_validate_result['status'] = False
        field_validate_result['message'] = '授信失败，开户行编码为空'
        return field_validate_result

    field_validate_result['status'] = True
    field_validate_result['message'] = ''
    return field_validate_result


# 通过身份证号获取年龄
def get_age_by_idcard_num(id_card):
    if len(id_card) == 18:
        id_birth_year = int(id_card[6:10])
        id_birth_month = int(id_card[10:12])
        id_birth_day = int(id_card[12:14])
    # 15位身份证情况
    elif len(id_card) == 15:
        id_birth_year_str = id_card[6:8]
        id_birth_month = int(id_card[8:10])
        id_birth_day = int(id_card[10:12])
        id_birth_year = int('19' + id_birth_year_str)
    else:
        return 0

    now = datetime.datetime.now()
    year = now.year
    month = now.month
    day = now.day

    if year == id_birth_year:
        return 0
    else:
        print("id_birth_month is %s" % id_birth_month)
        print("month is %s" % month)
        print("id_birth_day is %s" % id_birth_day)
        print("day is %s" % day)
        if (id_birth_month > month) or (id_birth_month == month and id_birth_day > day):
            return year - id_birth_year - 1
        else:
            return year - id_birth_year


def business_validate_data(application_data):
    business_validate_result = {}

    # 通过身份证号获取年龄
    # age = get_age_by_idcard_num(application_data['certificateNo'])
    # print("age is %s" % age)

    # if (age < 22) or (age > 55):
    #     business_validate_result['status'] = False
    #     business_validate_result['message'] = '年龄必须在22到55之间'
    #     return business_validate_result

    if application_data['applyAmount'] > 50000:
        business_validate_result['status'] = False
        business_validate_result['message'] = '授信金额不能大于5万'
        return business_validate_result

    business_validate_result['status'] = True
    business_validate_result['message'] = ''
    return business_validate_result


# 进入用信流程
def start_loan_contract(summary_uuid, veda_credit_no):
    asset_service = AssetService()
    return asset_service.create_loan_contract(summary_uuid, veda_credit_no)


def extract_tongdun_parama(application_data):
    tongdun_parama = {}
    tongdun_parama['productCode'] = application_data['productCode']
    tongdun_parama['applyId'] = application_data['merchantCreditNo']
    tongdun_parama['name'] = application_data['name']
    tongdun_parama['mobile'] = application_data['mobile']
    tongdun_parama['idNumber'] = application_data['certificateNo']
    tongdun_parama['loanAmount'] = application_data['applyAmount']
    return tongdun_parama


# 入口
def run_script(request_data):
    application_data = extract_application_data(request_data)
    print("获取到的application_data的内容是%s" % application_data)

    summary_service = SummaryService()

    borrower_uuid = create_borrower(application_data)
    print("创建借款用户，生成的borrower_uuid是=============>%s" % borrower_uuid)

    schedule_plan_uuid = request_data['schedulePlanUuid']

    # 校验字段
    field_validate_result = field_validate_data(application_data)

    if not field_validate_result['status']:
        update_credit_status(application_data['creditApplicationUuid'], 3, 0.00, borrower_uuid, None,
                             field_validate_result['message'].decode('UTF-8'))
        summary_service.update_summary_status(application_data['summaryAssetUuid'], 1,
                                              field_validate_result['message'].decode('UTF-8'))
        return

    # 业务校验
    business_validate_result = business_validate_data(application_data)

    if not business_validate_result['status']:
        # 授信失败，更新授信信息、资产进件信息，回调结果
        update_credit_status(application_data['creditApplicationUuid'], 3, 0.00, borrower_uuid, None,
                             business_validate_result['message'].decode('UTF-8'))
        summary_service.update_summary_status(application_data['summaryAssetUuid'], 1,
                                              field_validate_result['message'].decode('UTF-8'))
        return

    # 获取同盾请求参数
    tongdun_parama = extract_tongdun_parama(application_data)

    # todo 同盾
    tongdun_obj = RpcCall('YOULIWANG_TONGDUN_COMMON_CREDIT', schedule_plan_uuid, tongdun_parama)
    yield tongdun_obj.call()

    print("同盾风控的结果是%s" % tongdun_obj.response().rpc_result)
    if tongdun_obj.response().rpc_business_state == RESPONSE_BUSINESS_FAILED:
        tongdun_msg = tongdun_obj.response().rpc_result
        tongdun_msg_json = json.loads(tongdun_msg)
        tongdun_msg = tongdun_msg_json['analysed']
        tongdun_msg_json = json.loads(tongdun_msg)
        tongdun_msg = tongdun_msg_json["finalDecision"] + " , " + tongdun_msg_json["finalScore"] + " , " + \
                      tongdun_msg_json["riskName"]

        update_credit_status(application_data['creditApplicationUuid'], 3, 0.00, borrower_uuid, None,
                             tongdun_msg)
        summary_service.update_summary_status(application_data['summaryAssetUuid'], 1, tongdun_msg)
        return

    # 授信成功，更新授信状态回调
    update_credit_status(application_data['creditApplicationUuid'], 2, application_data['applyAmount'], borrower_uuid,
                         None,
                         field_validate_result['message'])

    # 调用用信接口
    result = start_loan_contract(application_data['summaryAssetUuid'], application_data['vedaCreditNo'])
    if not result['status']:
        # 用信失败
        summary_service.update_summary_status(application_data['summaryAssetUuid'], 2, result['message'])
        return


def create_engin(request_data):
    runtime = run_script(request_data)
    engine = AsyncEngine(runtime)
    result = engine.one_step()
    if "end" != result:
        return engine
    else:
        return result
