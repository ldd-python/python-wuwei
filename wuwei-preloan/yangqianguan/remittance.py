# coding=utf-8
import datetime
import json
import types
import time
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService
from com.suidifu.preloan.scheduler.service import SummaryService
from com.suidifu.preloan.scheduler.service import AssetService
from com.suidifu.preloan.scheduler.service import RemittanceService
from com.suidifu.preloan.scheduler.service import BorrowerService
from com.suidifu.preloan.scheduler.service import CreditLimitService
from com.suidifu.preloan.scheduler.service import PositionControlService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012


class async_engine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sendingMsg):
        if len(self.stack) == 0:
            return "end"
        msgToSend = sendingMsg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msgToSend == None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msgToSend)
                    msgToSend = None
                if (isinstance(ret, types.GeneratorType) == True):
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sendingMsg):
        return self.__one_step(sendingMsg)

    def OneStep(self, ):
        return self.__one_step(None)


class async_response:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_commnucation_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class async_request(object):

    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedulePlanUuid = schedulePlanUuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print "*response* -------->"
        print response
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = async_response(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                      response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if ('FAILED' == response_dic['businessStatus']):
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif ('SUCCEED' == response_dic['businessStatus']):
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if (self.rpc_processing_state != REQUEST_PENDING):
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while (self.rpc_processing_state == REQUEST_PROCESSING):
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield

            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        scheduleItemHandler = schedule_item_handler()
        request_uuid = scheduleItemHandler.send_item_rpc(self.rpc_channel_id, self.schedulePlanUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class schedule_item_handler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedulePlanUuid, rpc_params):
        print rpc_channel_id
        print rpc_params
        print type(rpc_params)
        print rpc_params.has_key('schedulePlanUuid')
        return self.sendItemRpc(rpc_channel_id, schedulePlanUuid, rpc_params)


class asset_service(AssetService):
    def __init__(self):
        pass

    def select_contract_by_merchant_contract_no(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def update_contract_status(self, contract_uuid, is_contract_validate_success, veda_credit_no, principalSum,
                               externalNoSection):
        return self.updateContractStatus(contract_uuid, is_contract_validate_success, veda_credit_no, principalSum,
                                         None)

    def update_contract_effective_date(self, merchant_contract_no, date):
        return self.updateContractStartDate(merchant_contract_no, date)

    def get_contract(self, merchantContractNo):
        contract_json = self.selectLoanContractByMerchantContractNo(merchantContractNo)
        contract = json.loads(contract_json)
        return contract


class remittance_service(RemittanceService):
    def __init__(self):
        pass

    def update_remittance_status(self, remittance_application_uuid, remittance_status, remark):
        return self.updateRemittanceStatus(remittance_application_uuid, remittance_status, remark)

    def update_bussiness_status(self, remittance_application_uuid, remittance_status, remark):
        return self.updateRemittanceBusinessStatus(remittance_application_uuid, remittance_status, remark)


class summary_service(SummaryService):
    def __init__(self):
        pass

    def update_summary_status(self, summary_asset_uuid, summary_status, remark):
        return self.updateSummaryAssetStatus(summary_asset_uuid, summary_status, remark)


class remittance_postloan_call(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(remittance_postloan_call, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(remittance_postloan_call, self).__eval_business_result(result)
        print "start [postloan_call] *result*-------->"
        print result


class call_back_remittance(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(call_back_remittance, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(call_back_remittance, self).__eval_business_result(result)
        print "start [call_back_remittance] *result*-------->"
        print result


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    summary_application = json.loads(request_content)
    return summary_application


def get_contract(application_data):
    assetService = asset_service()
    contact = assetService.select_contract_by_merchant_contract_no(application_data['merchantContractNo'])
    return contact

class position_service(PositionControlService):
    def __init__(self):
        pass

    def get_position_control(self, product_code):
        result_json = self.getPositionControl(product_code)
        result = json.loads(result_json)
        if result['code'] == '0000':
            return result['data']
        else:
            return None

# 校验数据
def validate_data(application_data):
    validate_result = {}
    contract = get_contract(application_data)
    print "contract is ------>"
    print contract
    positionService = position_service()
    position = positionService.get_position_control(application_data['productCode'])

    if contract is None:
        validate_result['status'] = False
        validate_result['message'] = "放款失败，合同不存在"
        return validate_result

    if contract['contractStatus'] != "AUDIT_SUCCEED":
        validate_result['status'] = False
        validate_result['message'] = "放款失败，合同状态未成功"
        return validate_result

    if application_data['loanAmount'] != contract['principalSum']:
        validate_result['status'] = False
        validate_result['message'] = "放款失败，放款金额与合同金额不一致"
        return validate_result

    if position is None:
        validate_result['status'] = True
        validate_result['message'] = ''
        print (position)
        return validate_result

    if position['switchStatus'] == 'ON':
        validate_result['status'] = True
        validate_result['message'] = ''
        print (position)
        return validate_result
    else:
        validate_result['status'] = False
        validate_result['message'] = '头寸拒绝'
        print (position)
        return validate_result

def update_remittance(application_data, validate_result):
    remittanceService = remittance_service()
    if not validate_result['status']:
        remittanceService.update_remittance_status(application_data['remittanceApplicationUuid'], 3, validate_result['message'])


def extract_call_postloan_data(application_data):
    try:
        contract = get_contract(application_data)
        call_postloan_data = {'merchantLoanOrderNo': application_data['merchantLoanOrderNo'],
                              'productCode': application_data['productCode'],
                              'vedaContractNo': contract['vedaContractNo'],
                              'merchantContractNo': application_data['merchantContractNo'],
                              'principalSum': application_data['loanAmount'],
                              'loanAmount': application_data['loanAmount'],
                              'loanStrategy': 0,
                              'loanDetail': application_data['remittanceApplicationDetails']}
        return call_postloan_data
    except Exception, e:
        print e
    finally:
        pass


def update_remittance_business_status(application_data, bussiness_status,borrower_uuid, product_code, remittance_uuid):
    remittanceService = remittance_service()
    credit_limit_service = CreditLimitService()
    if bussiness_status == RESPONSE_BUSINESS_OK:
        print "start update remittance success"
        remittanceService.update_remittance_status(application_data['remittanceApplicationUuid'], 2, None)
        # 将金额从冻结状态合并到放款中
        credit_limit_service.adjustEvent(borrower_uuid, product_code, remittance_uuid, 'REMITTANCE_SUCCEED')

    if bussiness_status == RESPONSE_BUSINESS_FAILED:
        print "start update remittance failed"
        remittanceService.update_remittance_status(application_data['remittanceApplicationUuid'], 3, None)
        # 将金额从冻结状态释放掉
        credit_limit_service.adjustEvent(borrower_uuid, product_code, remittance_uuid, 'REMITTANCE_FAILED')


def update_contract_bussiness_status(application_data, bussiness_status):
    assetService = asset_service()
    contract = get_contract(application_data)
    print "contract is -------->"
    print contract
    if bussiness_status is None:
        assetService.update_contract_status(contract['contractUuid'], 6, contract['vedaCreditNo'],
                                            application_data['loanAmount'], None)

    if bussiness_status == RESPONSE_BUSINESS_OK:
        assetService.update_contract_status(contract['contractUuid'], 7, contract['vedaCreditNo'],
                                            application_data['loanAmount'], None)
        effective_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        assetService.update_contract_effective_date(contract['merchantContractNo'], effective_date)

    if bussiness_status == RESPONSE_BUSINESS_FAILED:
        assetService.update_contract_status(contract['contractUuid'], 8, contract['vedaCreditNo'],
                                            application_data['loanAmount'], None)


def build_call_back_data(application_data, validate_result, business_state):
    call_back_data = {}
    contract = get_contract(application_data)
    call_back_data['applicationNo'] = contract['merchantCreditNo']
    loanDetails = application_data['remittanceApplicationDetails']
    detail = loanDetails[0]
    call_back_data['paymentNo'] = detail['businessRecordNo']
    call_back_data['paymentAmount'] = application_data['loanAmount']
    if not validate_result['status']:
        call_back_data['paymentStatus'] = '03'
    else:
        if business_state == RESPONSE_BUSINESS_OK:
            call_back_data['paymentStatus'] = '01'
            call_back_data['paymentTime'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            call_back_data['paymentTradeNo'] = ''

        if business_state == RESPONSE_BUSINESS_FAILED:
            call_back_data['paymentStatus'] = '03'
    call_back_data['gmtModified'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
    return call_back_data


def run_script(request_data):
    print "start [run_script] -------->"
    print "[run_script] success *request_data* -------->"
    print request_data
    print "start [extract_business_data] -------->"
    application_data = extract_business_data(request_data)
    print "[extract_business_data] success *application_data* -------->"
    print application_data
    scheduler_plan_uuid = request_data['schedulePlanUuid']
    # 校验放款信息
    validate_result = validate_data(application_data)
    print "validate_result -------- >"
    print validate_result
    update_remittance(application_data, validate_result)
    remittanceService = remittance_service()
    # 如果放款信息校验失败则return
    if not validate_result['status']:
        remittanceService.update_remittance_status(application_data['remittanceApplicationUuid'], 3, validate_result['message'].decode('ISO-8859-1'))
        return

    # 获取合同信息
    assetService = asset_service()
    contract = assetService.get_contract(application_data['merchantContractNo'])
    print 'the contract info is '
    print contract
    # 将金额设置为放款中
    print "start credit limit control"
    borrower_service = BorrowerService()
    veda_credit_no = contract['vedaCreditNo']
    borrwer_info_string = borrower_service.findBorrowerByVedaCreditNo(veda_credit_no)
    borrower_info = json.loads(borrwer_info_string)
    borrower_uuid = borrower_info['borrowerUuid']
    remittance_uuid = application_data['remittanceApplicationUuid']
    veda_credit_no = contract['vedaCreditNo']
    product_code = application_data['productCode']
    credit_limit_service = CreditLimitService()
    result = credit_limit_service.adjustEvent(borrower_uuid, product_code, remittance_uuid, 'REMITTANCE')
    result_json = json.loads(result)
    if ("0000" != result_json['code']):
        # 更新放款状态失败
        remittanceService.update_remittance_status(application_data['remittanceApplicationUuid'], 3, result_json['message'].decode('ISO-8859-1'))
        # 合同状态更新为放款失败
        # assetService.update_contract_status(contract['contractUuid'], 8, contract['vedaCreditNo'],
        #                                     contract['principalSum'], None, '用户额度冻结失败')
        return
    print "credit limit control end"
    # 放款指令同步贷后
    call_postloan_data = extract_call_postloan_data(application_data)
    print "start [remittance_postloan_call] -------->"
    update_contract_bussiness_status(application_data, None)
    rpc_remittance_req = remittance_postloan_call("YQG_REMITTANCE_POSTLOAN", scheduler_plan_uuid, call_postloan_data)
    yield rpc_remittance_req.call()
    print "[remittance_postloan_call] success *rpc_remittance_req* -------->"
    print rpc_remittance_req.response().rpc_business_state
    # 更新放款状态及资产进件状态
    print "start [update_remittance_business_status] start -------->"
    update_remittance_business_status(application_data, rpc_remittance_req.response().rpc_business_state,borrower_uuid, product_code, remittance_uuid)
    print "[update_remittance_business_status] success -------->"
    # 更新用信状态
    print "start [update_contract_bussiness_status] -------->"
    update_contract_bussiness_status(application_data, rpc_remittance_req.response().rpc_business_state)
    print "[update_contract_bussiness_status] success -------->"

    # print "start [call_back] --------->"
    # call_back_data = build_call_back_data(application_data, validate_result,
    #                                       rpc_remittance_req.response().rpc_business_state)
    # print call_back_data
    # call_back_req = call_back_remittance("JD_JD_REMITTANCE_CALLBACK", scheduler_plan_uuid, call_back_data)
    # call_back_req.emit()
    # print "[call_back] success -------->"



def create_engin(request_data):
    runtime = run_script(request_data)
    engine = async_engine(runtime)
    result = engine.OneStep()
    if "end" != result:
        return engine
    else:
        return result
