# coding=utf-8
import datetime
import json
import types
import time
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService
from com.suidifu.preloan.scheduler.service import SummaryService
from com.suidifu.preloan.scheduler.service import AssetService
from com.suidifu.preloan.scheduler.service import RemittanceService
from com.suidifu.preloan.scheduler.service import ProductService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012


class async_engine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sendingMsg):
        if len(self.stack) == 0:
            return "end"
        msgToSend = sendingMsg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msgToSend is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msgToSend)
                    msgToSend = None
                if isinstance(ret, types.GeneratorType):
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sendingMsg):
        return self.__one_step(sendingMsg)

    def OneStep(self, ):
        return self.__one_step(None)


class async_response:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_commnucation_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class async_request(object):

    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedulePlanUuid = schedulePlanUuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print "*response* -------->"
        print response
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = async_response(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                      response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if 'FAILED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif 'SUCCEED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield

            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        scheduleItemHandler = schedule_item_handler()
        request_uuid = scheduleItemHandler.send_item_rpc(self.rpc_channel_id, self.schedulePlanUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class schedule_item_handler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedulePlanUuid, rpc_params):
        print rpc_channel_id
        print rpc_params
        print type(rpc_params)
        print rpc_params.has_key('schedulePlanUuid')
        return self.sendItemRpc(rpc_channel_id, schedulePlanUuid, rpc_params)


class asset_service(AssetService):
    def __init__(self):
        pass

    def select_contract_by_merchant_contract_no(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def update_contract_status(self, contract_uuid, is_contract_validate_success, veda_credit_no, principalSum,
                               externalNoSection):
        return self.updateContractStatus(contract_uuid, is_contract_validate_success, veda_credit_no, principalSum,
                                         None)

    def update_contract_effective_date(self, merchant_contract_no, date):
        return self.updateContractStartDate(merchant_contract_no, date)

    def get_asset_sets_by_version(self, assetSetVersionNo):
        asset_set_list_json = self.getAllAssetSetByVersion(assetSetVersionNo)
        asset_set_list = json.loads(asset_set_list_json)
        return asset_set_list

    def get_last_asset_by_version(self, assetSetVersionNo):
        asset_set_json = self.getLastAssetByVersion(assetSetVersionNo)
        asset_set = json.loads(asset_set_json)
        return asset_set

    def update_asset_version_status(self, asset_set_version_uuid, asset_version_validate_status):
        return self.updateAssetSetVersionStatus(asset_set_version_uuid, asset_version_validate_status)

    def update_contract_expiration(self, merchantContractNo, expirationDate):
        return self.updateContractExpirationDate(merchantContractNo, expirationDate)

    def update_asset_version_status_after_loan(self, asset_set_version_uuid, asset_version_validate_status):
        return self.updateAssetSetVersionStatusAfterLoan(asset_set_version_uuid, asset_version_validate_status)


class credit_service(CreditService):
    def __init__(self):
        pass

    def get_credit_application(self, product_code, veda_credit_no):
        credit_json = self.getCreditApplicationByMerchantCreditNo(product_code, veda_credit_no)
        credit = json.loads(credit_json)
        return credit


class product_service(ProductService):
    def __init__(self):
        pass

    def select_product_by_uuid(self, product_uuid):
        product_json = self.getProductByUuid(product_uuid)
        product = json.loads(product_json)
        return product


class asset_set_postloan_call(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(asset_set_postloan_call, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(asset_set_postloan_call, self).__eval_business_result(result)
        print "__eval_business_result  rpc@asset_set_postloan_call %s" % self.rpc_channel_id
        pass


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    summary_application = json.loads(request_content)
    return summary_application


def get_contract(application_data):
    assetService = asset_service()
    contract = assetService.select_contract_by_merchant_contract_no(application_data['merchantContractNo'])
    return contract


def get_asset_sets(assetSetVersionNo):
    assetService = asset_service()
    asset_set_list = assetService.get_asset_sets_by_version(assetSetVersionNo)
    return asset_set_list


def get_last_asset(assetSetVersionNo):
    assetService = asset_service()
    asset_set = assetService.get_last_asset_by_version(assetSetVersionNo)
    return asset_set


def update_asset_version_business_status(application_data, validate_result):
    assetService = asset_service()
    if validate_result['status']:
        assetService.update_asset_version_status(application_data['assetSetVersionUuid'], True)
    else:
        assetService.update_asset_version_status(application_data['assetSetVersionUuid'], False)


# 校验数据
def validate_data(application_data):
    validate_result = {}
    print "start get contract --------->"
    contract = get_contract(application_data)
    print contract
    print "start get asset list"
    asset_set_list = get_asset_sets(application_data['versionNo'])
    print asset_set_list
    print "start check contract -------->"
    if contract is None:
        validate_result['status'] = False
        validate_result['message'] = "还款计划推送失败，不存在用信"
        return validate_result

    print "start check contract status -------->"
    if contract['contractStatus'] != "AUDIT_SUCCEED" and contract['contractStatus'] != "REMITTANCE_SUCCESS":
        validate_result['status'] = False
        validate_result['message'] = "还款计划推送失败，合同状态未成功"
        return validate_result

    print "start check amount -------->"
    principal_list = [asset_set.get('repaymentPrincipal') for asset_set in asset_set_list]
    principal_sum = 0
    for principal in principal_list:
        principal_sum = principal_sum + principal
    if abs(principal_sum - contract['principalSum']) >= 1.0e-9:
        validate_result['status'] = False
        validate_result['message'] = "还款计划推送失败，还款本金与合同本金不等"
        return validate_result

    # print "start check periodd count -------->"
    # if int(contract['repaymentPeriodsCount']) != len(asset_set_list):
    #     validate_result['status'] = False
    #     validate_result['message'] = "还款计划推送失败，还款计划个数与还款期数不等"
    #     return validate_result

    validate_result['status'] = True
    validate_result['message'] = ''
    return validate_result


def update_contract_expiration_date(application_data):
    assetService = asset_service()
    last_asset_set = get_last_asset(application_data['versionNo'])
    assetService.update_contract_expiration(application_data['merchantContractNo'],
                                            last_asset_set['plannedRepaymentDate'])


def extract_call_postloan_data(application_data):
    call_postloan_data = {}
    contract = get_contract(application_data)
    productService = product_service()
    product = productService.select_product_by_uuid(application_data['productUuid'])
    creditService = credit_service()
    credit = creditService.get_credit_application(product['productCode'], application_data['vedaCreditNo'])
    borrower_json = credit['borrowerApplicationInfo']
    borrower = json.loads(borrower_json)
    bankcardInfoSection_json = borrower['bankcardInfoSection']
    bankcardInfoSection = json.loads(bankcardInfoSection_json)
    loan_info_list_json = contract['loanInfoSection']
    loan_info_list = json.loads(loan_info_list_json)
    print "loan_info_list_json is --------------->"
    print loan_info_list_json

    # loan_info = loan_info_list[0]
    # 新合同规范
    # call_postloan_data['contractStandard'] = '2'
    call_postloan_data['vedaContractNo'] = application_data['vedaContractNo']
    print "1:" + application_data['vedaContractNo']
    call_postloan_data['merchantContractNo'] = application_data['merchantContractNo']
    print "2:" + application_data['vedaContractNo']
    call_postloan_data['vedaCreditNo'] = application_data['vedaCreditNo']
    print "3:" + application_data['vedaContractNo']
    call_postloan_data['borrowerUuid'] = credit['borrowerUuid']
    print "4:" + application_data['vedaContractNo']
    call_postloan_data['name'] = credit['name']
    print "5:" + application_data['vedaContractNo']
    call_postloan_data['certificateNo'] = credit['certificateNo']
    print "6:" + application_data['vedaContractNo']
    call_postloan_data['bankName'] = bankcardInfoSection['bankName']
    print "6-1:" + call_postloan_data['bankName']
    call_postloan_data['bankcardNo'] = bankcardInfoSection['bankcardNo']
    print "6-2:" + call_postloan_data['bankcardNo']
    call_postloan_data['principalSum'] = float(contract['principalSum'])
    print "7:" + str(contract['principalSum'])
    call_postloan_data['valueDate'] = contract['valueDate']
    print "8:" + contract['valueDate']
    call_postloan_data['expirationDate'] = contract['expirationDate']
    print "9:" + contract['expirationDate']
    call_postloan_data['interestRate'] = contract['interestRate']
    print "10:" + str(contract['interestRate'])
    call_postloan_data['interestRateCycle'] = contract['interestRateCycle']
    print "11:" + contract['interestRateCycle']
    call_postloan_data['repaymentMode'] = contract['repaymentMode']
    print "12:" + contract['repaymentMode']
    call_postloan_data['productCode'] = product['productCode']
    print "13:" + product['productCode']
    assetService = asset_service()
    asset_set_list = assetService.get_asset_sets_by_version(application_data['versionNo'])
    call_postloan_data['assetSet'] = asset_set_list
    print "14:"
    print call_postloan_data['assetSet']

    return call_postloan_data


def update_contract_bussiness_status(application_data, bussiness_status):
    assetService = asset_service()
    contract = get_contract(application_data)
    if bussiness_status == RESPONSE_BUSINESS_OK:
        assetService.update_contract_status(contract['contractUuid'], 9, contract['vedaCreditNo'],
                                            contract['principalSum'], None)


def update_asset_version_business_status_after_loan(application_data, validate_result):
    assetService = asset_service()
    if validate_result == RESPONSE_BUSINESS_OK:
        assetService.update_asset_version_status_after_loan(application_data['assetSetVersionUuid'], 4)


def run_script(request_data):
    print "start [run_script] -------->"
    print "*request_data* -------->"
    print request_data
    print "start [extract_business_data] -------->"
    application_data = extract_business_data(request_data)
    print "*application_data* -------->"
    print application_data
    # 校验还款推送数据
    print "start [validate_data] -------->"
    validate_result = validate_data(application_data)
    print "[validate_data] success *validate_result* -------->"
    print validate_result
    # 更新还款计划状态
    # validate_result = {'status': True, 'message': ''}
    print "start [update_asset_version_business_status] -------->"
    update_asset_version_business_status(application_data, validate_result)
    print "[update_asset_version_business_status] success -------->"
    if not validate_result['status']:
        return
    # 更新合同到期日期
    # print "start [update_contract_expiration_date] -------->"
    # update_contract_expiration_date(application_data)
    # print "[update_contract_expiration_date] success -------->"
    # 资产包导入贷后
    print "start [call_postloan_data] -------->"
    call_postloan_data = extract_call_postloan_data(application_data)
    print "[call_postloan_data] success *call_postloan_data* -------->"
    print call_postloan_data
    schedule_plan_uuid = request_data['schedulePlanUuid']
    print "start [asset_set_postloan_call] -------->"
    rpc_asset_req = asset_set_postloan_call("YQG_ASSET_SET_POSTLOAN", schedule_plan_uuid, call_postloan_data)
    yield rpc_asset_req.call()
    print "[asset_set_postloan_call] success *asset_set_postloan_call* -------->"
    print rpc_asset_req.response().rpc_business_state
    # 更新用信状态
    print "start [update_contract_bussiness_status] -------->"
    update_contract_bussiness_status(application_data, rpc_asset_req.response().rpc_business_state)
    print "[update_contract_bussiness_status] success -------->"

    # 更新还款计划推送状态
    print "start [update_asset_version_business_status_after_loan] -------->"
    update_asset_version_business_status_after_loan(application_data, rpc_asset_req.response().rpc_business_state)
    print "[update_asset_version_business_status_after_loan] success -------->"


def create_engin(request_data):
    try:
        runtime = run_script(request_data)
        engine = async_engine(runtime)
        result = engine.OneStep()
        if "end" != result:
            return engine
        else:
            return result
    except Exception, e:
        print e
    finally:
        pass
