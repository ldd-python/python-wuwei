# -*- coding: UTF-8 -*-
import json
import types
import base64
import time
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService, SummaryService, AssetService, ProductService, \
    PythonUtilsService, RemittanceService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'


class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class SummaryService(SummaryService):
    def __init__(self):
        pass

    def get_summary_by_uuid(self, summary_uuid):
        summery_vo = self.getSummaryAsset(summary_uuid)
        return json.loads(summery_vo)

    def update_summary_status(self, summary_asset_uuid, summary_status, remark, need_encode):
        if need_encode:
            remark = remark.encode('utf-8')
            remark_base64 = base64.b64encode(remark)
            return self.updateSummaryAssetStatusWithRemakBase64Encoded(summary_asset_uuid, summary_status,
                                                                       remark_base64)
        else:
            return self.updateSummaryAssetStatus(summary_asset_uuid, summary_status, remark)


class CreditService(CreditService):
    def __init__(self):
        pass

    def get_credit_application(self, product_code, veda_credit_no):
        credit_json = self.getCreditApplicationByVedaCreditNo(product_code, veda_credit_no)
        credit = json.loads(credit_json)
        return credit


class AssetService(AssetService):
    def __init__(self):
        pass

    def update_contract_status(self, contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                               contract_external_no, remark):
        return self.updateContractStatusAndRemark(contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                                                  contract_external_no, remark)


class ProductService(ProductService):
    def __init__(self):
        pass

    def get_product_by_code(self, product_code):
        return self.getProductByCode(product_code)


class PythonUtilsService(PythonUtilsService):
    def __init__(self):
        pass

    def add_months(self, date_time, months):
        return self.addMonths(date_time, months)


def contract_validate(application_data):
    contract_validate_result = {}

    # 字段校验
    if (not application_data.has_key('principalSum')) or application_data['principalSum'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同本金总计不能空'
        return contract_validate_result

    if (not application_data.has_key('interestSum')) or application_data['interestSum'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同利息总计不能空'
        return contract_validate_result

    if (not application_data.has_key('costSum')) or application_data['costSum'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同费用总计不能空'
        return contract_validate_result

    if (not application_data.has_key('signDate')) or application_data['signDate'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同签订日期不能空'
        return contract_validate_result

    if (not application_data.has_key('effectiveDate')) or application_data['effectiveDate'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同开始日期不能空'
        return contract_validate_result

    if (not application_data.has_key('expirationDate')) or application_data['expirationDate'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同到期日期为空'
        return contract_validate_result

    if (not application_data.has_key('valueDate')) or application_data['valueDate'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同起息日期'
        return contract_validate_result

    if (not application_data.has_key('interestRate')) or application_data['interestRate'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '签约利率不能空'
        return contract_validate_result

    if (not application_data.has_key('interestRateCycle')) or application_data['interestRateCycle'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '利率周期不能空'
        return contract_validate_result

    if (not application_data.has_key('payInterestCycle')) or application_data['payInterestCycle'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '付息周期不能空'
        return contract_validate_result

    if (not application_data.has_key('repaymentMode')) or application_data['repaymentMode'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '还款方式不能空'
        return contract_validate_result

    if (not application_data.has_key('repaymentPeriodsCount')) or application_data['repaymentPeriodsCount'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '还款期数不能空'
        return contract_validate_result

    if (not application_data.has_key('intendedUse')) or application_data['intendedUse'] is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '贷款用途不能空'
        return contract_validate_result

    # if application_data['principalSum'] > 20000:
    #     contract_validate_result['status'] = False
    #     contract_validate_result['message'] = '用信金额不能大于20000'
    #     return contract_validate_result

    print("商户授信编码是===========：%s" % application_data['merchantCreditNo'])

    credit_service = CreditService()

    # 根据商户申请编号和产品代码获取授信信息
    credit = credit_service.get_credit_application(application_data['productCode'],
                                                   application_data['merchantCreditNo'])
    print("credit的内容是===========%s" % credit)
    is_credit_exist = credit is not None
    if not is_credit_exist:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '找不到对应的授信信息'
        return contract_validate_result

    print("查询到的授信状态是========%s" % credit['creditStatus'])
    # 授信状态
    is_credit_success = credit['creditStatus'] == 2
    if not is_credit_success:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '没有对应的审核成功的授信信息'
        return contract_validate_result

    if application_data['principalSum'] > credit['applyAmount']:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同本金不能大于对应本次用信的授信申请金额'
        return contract_validate_result

    # 通过产品代码获取信托项目有效时间
    product_service = ProductService()
    product_str = product_service.get_product_by_code(application_data['productCode'])

    if product_str is None:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '根据产品代码找不到对应的产品项目'
        return contract_validate_result

    product = json.loads(product_str)

    product_start_date = product['startDate']

    contract_start_date = application_data['effectiveDate']
    contract_end_date = application_data['expirationDate']

    print("合同结束日期是%s" % contract_end_date)

    time_array = time.localtime(contract_end_date / 1000)
    dd = time.strftime("%Y-%m-%d", time_array)

    d = datetime.datetime.strptime(dd, "%Y-%m-%d")
    before_day = d + datetime.timedelta(days=0)
    print("提前的日期是%s", before_day)

    # 转成时间戳
    timestamp = int(time.mktime(before_day.timetuple()))
    unix_timestamp = timestamp * 1000
    print("提前时间转换成时间戳是%s", unix_timestamp)
    print("信托时间结束是%s", product['endDate'])

    if unix_timestamp > product['endDate']:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同结束日期必须早于信托结束日'
        return contract_validate_result

    if not contract_start_date >= product_start_date:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '合同开始时间必须在信托项目开始时间之后'
        return contract_validate_result

    print("application_data['interestRate'] is %s" % application_data['interestRate'])
    print("application_data['interestRateCycle'] is %s" % application_data['interestRateCycle'])
    # 利率周期默认是年化
    if (application_data['interestRateCycle'] is None) or (application_data['interestRateCycle'] == 'YEAR'):
        print("利率大小是%s" % application_data['interestRate'])
        if application_data['interestRate'] > 0.12:
            contract_validate_result['status'] = False
            contract_validate_result['message'] = '用信失败，签约利率不正确'
            return contract_validate_result

    # 月利率 ，签约利率*12
    if application_data['interestRateCycle'] == 'MONTH':
        if application_data['interestRate'] * 12 > 0.12:
            contract_validate_result['status'] = False
            contract_validate_result['message'] = '用信失败，签约利率不正确'
            return contract_validate_result

    # 日利率 ，签约利率*360
    if application_data['interestRateCycle'] == 'DAY':
        if application_data['interestRate'] * 360 > 0.12:
            contract_validate_result['status'] = False
            contract_validate_result['message'] = '用信失败，签约利率不正确'
            return contract_validate_result

    # loan_periods_list = [1, 3, 6, 9, 12]
    #
    # print application_data['repaymentPeriodsCount'] not in loan_periods_list
    # if application_data['repaymentPeriodsCount'] not in loan_periods_list:
    #     contract_validate_result['status'] = False
    #     contract_validate_result['message'] = '用信失败，贷款期数不符合要求'
    #     return contract_validate_result
    #
    # if application_data['repaymentMode'] != 'EQUALITY_CORPUS_AND_INTEREST':
    #     contract_validate_result['status'] = False
    #     contract_validate_result['message'] = '用信失败，还款方式必须是等额本息'
    #     return contract_validate_result

    # 合同到期日期减去合同起息日期不超过18个月(合同起息日加上18个月大于等于合同到期日)
    python_utils_service = PythonUtilsService()
    time_array = time.localtime(application_data['valueDate'] / 1000)
    dd = time.strftime("%Y-%m-%d", time_array)
    added_time = python_utils_service.add_months(dd, 18)

    d = datetime.datetime.strptime(added_time, "%Y-%m-%d")
    timestamp = int(time.mktime(d.timetuple()))
    unix_timestamp = timestamp * 1000

    if application_data['expirationDate'] > unix_timestamp:
        contract_validate_result['status'] = False
        contract_validate_result['message'] = '用信失败，合同到期日与合同起息日之间不能相差超过18个月'
        return contract_validate_result

    contract_validate_result['status'] = True
    contract_validate_result['message'] = ''

    return contract_validate_result


def extract_application_data(request_data):
    request_content = request_data['requestContent']
    application = json.loads(request_content)
    return application


# 更新用信信息
def update_contract_validate_status(contract_uuid, contract_status, vedaCreditNo, principalSum,
                                    contract_external_no, remark):
    asset_service = AssetService()
    print contract_status
    asset_service.update_contract_status(contract_uuid, contract_status, vedaCreditNo, principalSum,
                                         contract_external_no, remark)


def run_script(request_data):
    application_data = extract_application_data(request_data)
    print("获取到的application_data的内容是%s" % application_data)

    summary_service = SummaryService()

    contract_validate_result = contract_validate(application_data)

    if not contract_validate_result['status']:
        # 用信失败，更新用信状态、资产进件状态、回调
        update_contract_validate_status(application_data['contractUuid'], 3, application_data['vedaCreditNo'],
                                        application_data['principalSum'], None, contract_validate_result['message'])
        # 更新summary状态
        summary_service.update_summary_status(application_data['summaryAssetUuid'], 2,
                                              contract_validate_result['message'], False)
    else:
        # 用信成功，修改用信状态、资产进件成功、回调
        update_contract_validate_status(application_data['contractUuid'], 2, application_data['vedaCreditNo'],
                                        application_data['principalSum'], None, contract_validate_result['message'])

        # 更新summary状态
        summary_service.update_summary_status(application_data['summaryAssetUuid'], 6,
                                              '进件成功', False)


def create_engin(request_data):
    print("enter create_engin...")
    run_script(request_data)
    return "end"
