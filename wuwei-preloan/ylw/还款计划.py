# -*- coding: UTF-8 -*-
import json
import types
import time
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService, AssetService, ProductService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'


class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class CreditService(CreditService):
    def __init__(self):
        pass

    def get_credit_application(self, product_code, veda_credit_no):
        return self.getCreditApplicationByMerchantCreditNo(product_code, veda_credit_no)


class AssetService(AssetService):
    def __init__(self):
        pass

    def get_contract(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def get_last_asset_set(self, version_no):
        return self.getLastAssetByVersion(version_no)

    def get_contract(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def update_asset_version_status(self, asset_set_version_uuid, asset_version_validate_status, remark):
        return self.updateAssetSetVersionStatusAndRemark(asset_set_version_uuid, asset_version_validate_status, remark)

    def get_asset_sets_json(self, version_no):
        return self.getAllAssetSetByVersion(version_no)

    def update_contract_status(self, contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                               contract_external_no, remark):
        return self.updateContractStatusAndRemark(contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                                                  contract_external_no, remark)

    def update_contract_expiration(self, merchantContractNo, expirationDate):
        return self.updateContractExpirationDate(merchantContractNo, expirationDate)


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class ProductService(ProductService):
    def __init__(self):
        pass

    def get_product_by_uuid(self, product_uuid):
        return self.getProductByUuid(product_uuid)


class RpcCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        super(RpcCall, self).__init__(gateway_channel_id, schedule_plan_uuid, params)

    def __eval_business_result(self, result):
        super(RpcCall, self).__eval_business_result(result)
        print("__eval_business_result  rpc@contract_callback %s" % self.rpc_channel_id)


# 调用贷后
def extract_call_postloan_data(application_data):
    call_postloan_data = {}

    asset_service = AssetService()
    credit_service = CreditService()

    contract = asset_service.get_contract(application_data['merchantContractNo'])


    print("contract的值是==========%s" % contract)

    product_service = ProductService()
    product_str = product_service.get_product_by_uuid(application_data['productUuid'])

    product = json.loads(product_str)
    credit_info_json = credit_service.get_credit_application(product['productCode'],application_data['vedaCreditNo'])
    credit_info = json.loads(credit_info_json)

    borrower_json = credit_info['borrowerApplicationInfo']
    borrower = json.loads(borrower_json)
    bankcardInfoSection_json = borrower['bankcardInfoSection']
    bankcardInfoSection = json.loads(bankcardInfoSection_json)

    call_postloan_data['bankName'] = bankcardInfoSection['bankName']
    call_postloan_data['bankcardNo'] = bankcardInfoSection['bankcardNo']
    call_postloan_data['vedaContractNo'] = application_data['vedaContractNo']
    call_postloan_data['merchantContractNo'] = application_data['merchantContractNo']
    call_postloan_data['vedaCreditNo'] = application_data['vedaCreditNo']
    call_postloan_data['principalSum'] = contract['principalSum']
    call_postloan_data['valueDate'] = contract['valueDate']
    call_postloan_data['expirationDate'] = contract['expirationDate']
    call_postloan_data['interestRate'] = contract['interestRate']
    call_postloan_data['interestRateCycle'] = contract['interestRateCycle']
    call_postloan_data['repaymentMode'] = contract['repaymentMode']
    call_postloan_data['productCode'] = product['productCode']
    call_postloan_data['certificateNo'] = credit_info['certificateNo']
    call_postloan_data['borrowerUuid'] = credit_info['borrowerUuid']
    call_postloan_data['name'] = credit_info['name']
    call_postloan_data['mobile'] = credit_info['mobile']


    asset_set_list = asset_service.get_asset_sets_json(application_data['versionNo'])
    call_postloan_data['assetSet'] = asset_set_list

    return call_postloan_data

def get_asset_sets(assetSetVersionNo):
    asset_service = AssetService()
    asset_set_list = asset_service.getAllAssetSetByVersion(assetSetVersionNo)
    return asset_set_list


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    asset_version = json.loads(request_content)
    return asset_version


def update_asset_version_validate_status(asset_set_version_uuid, asset_version_validate_status, remark):
    asset_service = AssetService()
    asset_service.update_asset_version_status(asset_set_version_uuid, asset_version_validate_status, remark)


def dateToTime(date,clas):
    if(clas==0):
        timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
        timeStamp = int(time.mktime(timeArray))
        print date,"时间转换为：",timeStamp
        return timeStamp
    if(clas==1):
        timeArray = time.strptime(date, "%Y-%m-%d")
        timeStamp = int(time.mktime(timeArray))
        print date,"时间转换为：",timeStamp
        return timeStamp

def asset_set_validate_data(application_data):
    asset_set_validate_result = {}

    product_service = ProductService()
    product_str = product_service.get_product_by_uuid(application_data['productUuid'])

    if product_str is None:
        asset_set_validate_result['status'] = False
        asset_set_validate_result['message'] = '根据产品代码找不到对应的产品项目'
        return asset_set_validate_result

    asset_service = AssetService()
    asset_set_str = asset_service.get_last_asset_set(application_data['versionNo'])

    if asset_set_str is None:
        asset_set_validate_result['status'] = False
        asset_set_validate_result['message'] = '找不到对应的还款计划'
        return asset_set_validate_result

    product = json.loads(product_str)

    product_end_date = product['endDate']

    contract = asset_service.get_contract(application_data['merchantContractNo'])

    asset_set_list = get_asset_sets(application_data['versionNo'])
    asset_set_list_obj = json.loads(asset_set_list)
    print "asset_set_list_obj", asset_set_list_obj
    print "contract", contract

    # 第一期还款计划时间
    start_time = '1970-01-01'
    if not asset_set_list_obj is None:
        start_time = asset_set_list_obj[0]['plannedRepaymentDate']


    principal_list = []
    for asset_set in asset_set_list_obj:
        principal_list.append(asset_set['repaymentPrincipal'])

    principal_sum=0
    for principal in principal_list:
        principal_sum = principal_sum + principal
    if abs(principal_sum-contract['principalSum']) >= 1.0e-9:
        asset_set_validate_result['status'] = False
        asset_set_validate_result['message'] = "还款计划推送失败，还款本金与合同本金不等"
        return asset_set_validate_result

    asset_set = json.loads(asset_set_str)
    last_repay_date_str = asset_set['plannedRepaymentDate']

    print("最后一期还款计划时间是%s" % last_repay_date_str)

    d = datetime.datetime.strptime(last_repay_date_str, "%Y-%m-%d")
    before_day = d + datetime.timedelta(days=0)
    print("提前的日期是%s",before_day)

    # 转成时间戳
    timestamp = int(time.mktime(before_day.timetuple()))
    unix_timestamp = timestamp * 1000
    print("提前时间转换成时间戳是%s",unix_timestamp)
    print("信托时间结束是%s",product_end_date)

    if unix_timestamp > product_end_date:
        asset_set_validate_result['status'] = False
        asset_set_validate_result['message'] = '最后一期还款计划不能晚于信托结束日'
        return asset_set_validate_result

    effectiveDate = contract['effectiveDate']
    expirationDate = contract['expirationDate']

    effectiveDate = dateToTime(effectiveDate, 0)
    expirationDate = dateToTime(expirationDate, 0)
    unix_timestamp = dateToTime(last_repay_date_str, 1)
    start_time = dateToTime(start_time, 1)

    # 最后一期还款时间不能大于合同到期时间
    if unix_timestamp > expirationDate:
        print('the last asset time:',unix_timestamp)
        print('contract over time:s%',expirationDate)
        asset_set_validate_result['status'] = False
        asset_set_validate_result['message'] = '最后一期还款时间不能大于合同到期时间'
        return asset_set_validate_result

    # 第一期还款时间不能小于合同开始时间
    if start_time < effectiveDate:
        print('the first asset time:',start_time)
        print('contract start time:',effectiveDate)
        asset_set_validate_result['status'] = False
        asset_set_validate_result['message'] = '第一期还款时间不能小于合同开始时间'
        return asset_set_validate_result

    asset_set_validate_result['status'] = True
    asset_set_validate_result['message'] = ''
    return asset_set_validate_result


def run_script(request_data):
    application_data = extract_business_data(request_data)
    print("application_data的内容是%s" % application_data)

    # 还款计划业务校验
    asset_set_validate_result = asset_set_validate_data(application_data)

    print("还款计划校验的结果是%s" % asset_set_validate_result)
    update_asset_version_validate_status(application_data['assetSetVersionUuid'], asset_set_validate_result['status'],
                                         asset_set_validate_result['message'])

    schedule_plan_uuid = request_data['schedulePlanUuid']

    if asset_set_validate_result['status']:
        # 资产包导入贷后
        call_postloan_data = extract_call_postloan_data(application_data)
        print("call_postloan_data----->%s" % call_postloan_data)

        asset_service = AssetService()

        rpc_asset_set_obj = RpcCall("YOULIWANG_ASSET_SET_POSTLOAN", schedule_plan_uuid, call_postloan_data)
        yield rpc_asset_set_obj.call()
        print("调用贷后的反馈结果是%s" % rpc_asset_set_obj.response().rpc_result)

        # 获取合同信息
        contract = asset_service.get_contract(application_data['merchantContractNo'])

        # 资产包导入贷后成功，将合同状态置为已生效
        if rpc_asset_set_obj.response().rpc_business_state == RESPONSE_BUSINESS_OK:
            asset_service.update_contract_status(contract['contractUuid'], 9, contract['vedaCreditNo'],
                                                 contract['principalSum'], None, '还款计划导入成功')


def create_engin(request_data):
    runtime = run_script(request_data)
    engine = AsyncEngine(runtime)
    result = engine.one_step()
    if "end" != result:
        return engine
    else:
        return result
