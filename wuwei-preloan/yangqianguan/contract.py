# coding=utf-8
import json
import types
import time
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService
from com.suidifu.preloan.scheduler.service import SummaryService
from com.suidifu.preloan.scheduler.service import AssetService
from com.suidifu.preloan.scheduler.service import ProductService
from com.suidifu.preloan.scheduler.service import RemittanceService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012


class async_engine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sendingMsg):
        if len(self.stack) == 0:
            return "end"
        msgToSend = sendingMsg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msgToSend is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msgToSend)
                    msgToSend = None
                if isinstance(ret, types.GeneratorType):
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sendingMsg):
        return self.__one_step(sendingMsg)

    def OneStep(self, ):
        return self.__one_step(None)


class async_response:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_commnucation_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class async_request(object):

    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedulePlanUuid = schedulePlanUuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print "*response* -------->"
        print response
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = async_response(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                      response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if 'FAILED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif 'SUCCEED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield

            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        scheduleItemHandler = schedule_item_handler()
        request_uuid = scheduleItemHandler.send_item_rpc(self.rpc_channel_id, self.schedulePlanUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class schedule_item_handler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedulePlanUuid, rpc_params):
        print rpc_channel_id
        print rpc_params
        print type(rpc_params)
        print rpc_params.has_key('schedulePlanUuid')
        return self.sendItemRpc(rpc_channel_id, schedulePlanUuid, rpc_params)


class product_service(ProductService):
    def __init__(self):
        pass

    def select_product_by_code(self, product_code):
        product_json = self.getProductByCode(product_code)
        product = json.loads(product_json)
        return product


class asset_service(AssetService):
    def __init__(self):
        pass

    def get_success_loan_contract_list(self, veda_credit_no, merchant_contract_no):
        loanContractListJson = self.getProcessingLoanContractByVedaCreditNo(veda_credit_no, merchant_contract_no)
        loanContractList = json.loads(loanContractListJson)
        return loanContractList

    def update_contract_status(self, contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                               contract_external_no):
        return self.updateContractStatus(contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                                         contract_external_no)

    def update_contract_remark(self, contract_uuid, contract_status_ordinal, remark):
        return self.updateContractRemark(contract_uuid, contract_status_ordinal, remark)


class credit_service(CreditService):
    def __init__(self):
        pass

    def select_credit_by_merchant_credit_no(self, product_code, merchant_credit_no):
        credit_json = self.getCreditApplicationByVedaCreditNo(product_code, merchant_credit_no)
        credit = json.loads(credit_json)
        return credit


class remittance_service(RemittanceService):
    def __init__(self):
        pass

    def create_remittance_application(self, summary_asset_uuid, veda_contract_no):
        result_json = self.creatRemittanceApplication(summary_asset_uuid, veda_contract_no)
        result = json.loads(result_json)
        return result


class summary_service(SummaryService):
    def __init__(self):
        pass

    def update_summary_status(self, summary_asset_uuid, summary_status, remark):
        return self.updateSummaryAssetStatus(summary_asset_uuid, summary_status, remark)

    def get_summary_by_uuid(self, summary_asset_uuid):
        summary_json = self.getSummaryAsset(summary_asset_uuid)
        return json.loads(summary_json)


class summary_call_back(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(summary_call_back, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(summary_call_back, self).__eval_business_result(result)
        print "__eval_business_result  rpc@summary_call_back %s" % self.rpc_channel_id
        pass


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    summary_application = json.loads(request_content)
    return summary_application


def get_credit(product_code, merchant_credit_no):
    creditService = credit_service()
    credit = creditService.select_credit_by_merchant_credit_no(product_code, merchant_credit_no)
    return credit


def get_success_contract(veda_credit_no, merchant_contract_no):
    assetService = asset_service()
    loan_contract_list = assetService.get_success_loan_contract_list(veda_credit_no, merchant_contract_no)
    return loan_contract_list


def get_product(product_code):
    productService = product_service()
    product = productService.select_product_by_code(product_code)
    return product


def get_summary_asset(summary_asset_uuid):
    summaryService = summary_service()
    return summaryService.get_summary_by_uuid(summary_asset_uuid)

def betMonth(begindate,enddate):
    begindateArray = datetime.datetime.fromtimestamp(begindate)
    enfdateArray = datetime.datetime.fromtimestamp(enddate)
    print ("begindateArray", begindateArray, "enfdateArray", enfdateArray)
    diffyear = enfdateArray.year - begindateArray.year
    diffmonth = enfdateArray.month - begindateArray.month
    months = (diffyear * 12) + diffmonth
    print ("betmoths", months)
    return months


# 校验数据
def validate_data(application_data):
    validate_result = {}
    print "start [get_credit] -------->"
    product_code = application_data['productCode']
    merchant_credit_no = application_data['merchantCreditNo']
    credit = get_credit(product_code, merchant_credit_no)
    print "[get_credit] success *credit* -------->"
    print credit
    loan_contract_list = get_success_contract(application_data['vedaCreditNo'], application_data['merchantContractNo'])
    product = get_product(application_data['productCode'])

    # 校验合同本金总计
    if (not application_data['principalSum']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，合同本金为空"
        return validate_result
    # 校验签订日期
    if (not application_data['signDate']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，签订日期为空"
        return validate_result
    # 校验开始日期
    if (not application_data['effectiveDate']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，开始日期为空"
        return validate_result
    # 校验到期日期
    if (not application_data['expirationDate']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，到期日期为空"
        return validate_result
    # 校验起息日期
    if (not application_data['valueDate']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，起息日期为空"
        return validate_result
    # 校验签约利率
    if (not application_data['interestRate']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，签约利率为空"
        return validate_result
    # 校验利率周期
    if (not application_data['interestRateCycle']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，利率周期为空"
        return validate_result
    # 校验还款方式
    if (not application_data['repaymentMode']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，还款方式为空"
        return validate_result
    # 校验贷款用途
    if (not application_data['intendedUse']):
        validate_result['status'] = False
        validate_result['message'] = "用信失败，贷款用途"
        return validate_result



    # 校验授信是否存在
    if credit is None:
        validate_result['status'] = False
        validate_result['message'] = "用信失败，不存在授信"
        return validate_result
    # 校验授信状态是否成功
    if credit['creditStatus'] != 2:
        validate_result['status'] = False
        validate_result['message'] = "用信失败，授信状态未成功"
        return validate_result
    # 校验授信下是否存在未失败的用信
    if len(loan_contract_list) != 0:
        validate_result['status'] = False
        validate_result['message'] = "用信失败，该授信下存在未失败的用信"
        return validate_result

    # 校验合同年化利率是否小于36%
    if application_data.has_key('interestRateCycle') and application_data.has_key('interestRate'):
        interest_rate_cycle = application_data['interestRateCycle']
        interest_rate = float(application_data['interestRate'])
        if interest_rate_cycle == 'YEAR':
            interest_rate = interest_rate
        elif interest_rate_cycle == 'MONTH':
            interest_rate = interest_rate * 12
        elif interest_rate_cycle == 'DAY':
            interest_rate = interest_rate * 360
        else:
            validate_result['status'] = False
            validate_result['message'] = "用信失败，利率格式不正确"
            return validate_result

        if interest_rate > 0.36:
            validate_result['status'] = False
            validate_result['message'] = '用信失败，年化利率大于36%'
            return validate_result
    # 校验合同结束日期是否晚于信托截止日期

    if (application_data.has_key('effectiveDate')):
        # 校验申请贷款期限不能超过12个月
        # 合同开始日期
        begindate = application_data['effectiveDate'] / 1000
        print ("begindate", begindate)
        # 合同结束日期
        enddate = application_data['expirationDate'] / 1000
        print ("enddate", enddate)
        months = betMonth(begindate, enddate)
        if (months > 12):
            validate_result['status'] = False
            validate_result['message'] = "用信失败，申请贷款期限不能超过12个月"
            return validate_result
        # effective_date = time.localtime(long(application_data['gmtCreate'])/1000)
        # effective_date = time.strftime("%Y-%m-%d", effective_date)
        # effective_date = str(effective_date)
        # effective_year = effective_date[0 : 4]
        # effective_month = effective_date[5 : 7]
        # effective_day = effective_date[8 : 10]
        #
        # end_year = effective_year
        # end_month = effective_month
        # end_day = effective_day
        # if (int(effective_month) + repayment_periods_count > 12 ):
        #     end_year = int(effective_year) + 1
        #     end_month = int(effective_month) + repayment_periods_count - 12
        # else:
        #     end_year = int(effective_year)
        #     end_month = int(effective_month) + repayment_periods_count
        #
        # end_day = int(effective_day)
        # if (end_month == 2 and end_day > 28):
        #     end_day = 28
        #
        # if(end_month == 4 or end_month == 6 or end_month == 9 or end_month == 11):
        #     if(end_day == 31):
        #         end_day = 30
        #
        # end_date = str(end_year) + '-' + str(end_month) + '-' + str(end_day)
        # # 获取商户合同终止日期
        product_end_date = time.localtime(long(str(product['endDate']))/1000)
        # product_end_date = time.strftime("%Y-%m-%d", product_end_date)
        # product_end_date = str(product_end_date)
        # product_end_date = datetime.datetime.strptime(product_end_date, "%Y-%m-%d")
        # contract_end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
        if (enddate > product_end_date):
            validate_result['status'] = False
            validate_result['message'] = '用信失败，还款结束日期大于合同结束日期'
            return validate_result
    else:
        validate_result['status'] = False
        validate_result['message'] = '用信失败，合同开始日期为空'
        return validate_result

    validate_result['status'] = True
    validate_result['message'] = ''
    return validate_result


def update_contract(application_data, validate_result):
    assetService = asset_service()
    if validate_result['status']:
        assetService.update_contract_status(application_data['contractUuid'], 2, application_data['vedaCreditNo'],
                                            application_data['principalSum'], None)
        assetService.update_contract_remark(application_data['contractUuid'], 2, validate_result['message'])
    else:
        assetService.update_contract_status(application_data['contractUuid'], 3, application_data['vedaCreditNo'],
                                            application_data['principalSum'], None)
        assetService.update_contract_remark(application_data['contractUuid'], 3, validate_result['message'])


def create_remittance(summary_asset_uuid, veda_contract_no):
    remittanceService = remittance_service()
    result = remittanceService.create_remittance_application(summary_asset_uuid, veda_contract_no)
    return result


def update_summary(summary_asset_uuid, summary_status, remark):
    summaryService = summary_service()
    summaryService.update_summary_status(summary_asset_uuid, summary_status, remark)


def run_script(request_data):
    print "start [run_script] -------->"
    print "[run_script] success *request_data* -------->"
    print request_data
    print "start [extract_business_data] -------->"
    application_data = extract_business_data(request_data)
    print "[extract_business_data] success *application_data* -------->"
    print application_data
    scheduler_plan_uuid = request_data['schedulePlanUuid']
    summary_asset_uuid = application_data['summaryAssetUuid']
    summary_asset = get_summary_asset(summary_asset_uuid)
    # 校验用信数据
    print "start [validate_data] -------->"
    validate_result = validate_data(application_data)
    print "[validate_data] success *validate_result* -------->"
    print validate_result
    # validate_result = {'status': True, 'message': ''}

    # 更新用信状态
    print "start [update_contract] -------->"
    update_contract(application_data, validate_result)
    print "[update_contract] success -------->"
    print "start [update_summary] -------->"
    if not validate_result['status']:
        update_summary(application_data['summaryAssetUuid'], 2, validate_result['message'])
    else:
        update_summary(application_data['summaryAssetUuid'], 6, "")
    print "[update_summary] success -------->"



def create_engin(request_data):
    runtime = run_script(request_data)
    engine = async_engine(runtime)
    return "end"
