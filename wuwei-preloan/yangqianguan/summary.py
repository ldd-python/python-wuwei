# coding=utf-8
import datetime
import json
import types
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService
from com.suidifu.preloan.scheduler.service import SummaryService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012


class async_engine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sendingMsg):
        if len(self.stack) == 0:
            return "end"
        msgToSend = sendingMsg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msgToSend is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msgToSend)
                    msgToSend = None
                if isinstance(ret, types.GeneratorType):
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sendingMsg):
        return self.__one_step(sendingMsg)

    def OneStep(self, ):
        return self.__one_step(None)


class async_response:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_commnucation_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class async_request(object):

    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedulePlanUuid = schedulePlanUuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print "*response* -------->"
        print response
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = async_response(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                      response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if 'FAILED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif 'SUCCEED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield

            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        scheduleItemHandler = schedule_item_handler()
        request_uuid = scheduleItemHandler.send_item_rpc(self.rpc_channel_id, self.schedulePlanUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class schedule_item_handler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedulePlanUuid, rpc_params):
        print rpc_channel_id
        print rpc_params
        print type(rpc_params)
        print rpc_params.has_key('schedulePlanUuid')
        return self.sendItemRpc(rpc_channel_id, schedulePlanUuid, rpc_params)


class credit_service(CreditService):
    def __init__(self):
        pass

    def create_credit_application(self, summary_asset_json):
        result_json = self.createCredit(summary_asset_json)
        result = json.loads(result_json)
        return result


class summary_service(SummaryService):
    def __init__(self):
        pass

    def update_summary_status(self, summary_asset_uuid, summary_status, remark):
        return self.updateSummaryAssetStatus(summary_asset_uuid, summary_status, remark)


class summary_call_back(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(summary_call_back, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(summary_call_back, self).__eval_business_result(result)
        print "__eval_business_result  rpc@summary_call_back %s" % self.rpc_channel_id
        pass


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    summary_application = json.loads(request_content)
    return summary_application


def create_credit(summary_asset_uuid):
    creditService = credit_service()
    result = creditService.create_credit_application(summary_asset_uuid)
    return result


def update_summary(summary_asset_uuid, summary_status, remark):
    summaryService = summary_service()
    summaryService.update_summary_status(summary_asset_uuid, summary_status, remark)


def build_callback_data(application_data, result):
    callback_data = {'orderNo': application_data['requestNo'],
                     'quotaNo': application_data['summaryNo'],
                     'status': 'failed',
                     'remark': result['message']}
    return callback_data


def run_script(request_data):
    print "start [run_script] -------->"
    print "*request_data* -------->"
    print request_data
    print "start [extract_business_data] -------->"
    application_data = extract_business_data(request_data)
    print "*application_data* -------->"
    print application_data
    schedule_plan_uuid = request_data['schedulePlanUuid']
    # 校验资产进件数据

    # 开始创建授信
    print "start [create_credit] -------->"
    summary_asset_json = json.dumps(application_data)
    result = create_credit(summary_asset_json)
    print "credit_result -------->"
    print result
    if result['code'] != '0000':
        print "start [update_summary] -------->"
        update_summary(application_data['assetUuid'], 1, result['message'])
        print "[update_summary] success -------->"
        print "start [summary_call_back] -------->"
        callback_data = build_callback_data(application_data, result)
        callback_req = summary_call_back("JD_JD_SUMMARY_CALLBACK", schedule_plan_uuid, callback_data)
        callback_req.emit()
        print "[summary_call_back] success -------->"


def create_engin(request_data):
    runtime = run_script(request_data)
    engine = async_engine(runtime)
    return "end"
