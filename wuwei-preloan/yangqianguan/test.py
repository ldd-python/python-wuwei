# -*- coding = UTF-8 -*-
import json
import types
import time
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import AssetService
from com.suidifu.preloan.scheduler.service import CreditService
from com.suidifu.preloan.scheduler.service import ProductService
from com.suidifu.preloan.scheduler.service import RemittanceService
from com.suidifu.preloan.scheduler.service import SummaryService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'


class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def __eval_business_result(self, result):
        print("__eval_business_result  rpc 是：%s" % self.rpc_channel_id)
        return RESPONSE_BUSINESS_OK

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AssetSetPostloanCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        super(AssetSetPostloanCall, self).__init__(gateway_channel_id, schedule_plan_uuid, params)

    def __eval_business_result(self, result):
        super(AssetSetPostloanCall, self).__eval_business_result(result)
        print "__eval_business_result  rpc@asset_set_postloan_call %s" % self.rpc_channel_id


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        print("type(rpc_params)的内容是%s" % type(rpc_params))
        print("rpc_params.has_key('schedulePlanUuid')的内容是%s" % rpc_params.has_key('schedulePlanUuid'))
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class ProductService(ProductService):
    def __init__(self):
        pass

    def get_product(self, product_uuid):
        product_json = self.getProductByUuid(product_uuid)
        product = json.loads(product_json)
        return product

class SummaryService(SummaryService):
    def __init__(self):
        pass

    def update_summary_asset_status(self,asset_uuid,credit_data,remark):
        self.updateSummaryAssetStatus(asset_uuid,credit_data,remark)
    def get_summary_asset(self,summary_asset_uuid):
        return self.getSummaryAsset(summary_asset_uuid)
class AssetService(AssetService):
    def __init__(self):
        pass

    def update_contract_status(self, veda_contract_no, status,remark):
        return self.updateContractStatusByVedaContractNo(veda_contract_no, status,remark)

    def get_contract(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def get_asset_sets(self, asset_set_version_no):
        asset_set_list_json = self.getAllAssetSetByVersion(asset_set_version_no)
        asset_set_list = json.loads(asset_set_list_json)
        return asset_set_list

    def get_asset_sets_json(self, asset_set_version_no):
        asset_set_list_json = self.getAllAssetSetByVersion(asset_set_version_no)
        return asset_set_list_json

    def update_asset_version_status(self, asset_set_version_uuid, asset_version_validate_status,remark):
        return self.updateAssetSetVersionStatusAndRemark(asset_set_version_uuid, asset_version_validate_status,remark)

    def update_contract_status(self,veda_contract_no,status,remark):
        self.updateContractStatusByVedaContractNo(veda_contract_no,status,remark)

class CreditService(CreditService):
    def __init__(self):
        pass

    def get_credit_application(self, product_code, veda_credit_no):
        credit_json = self.getCreditApplicationByMerchantCreditNo(product_code, veda_credit_no)
        credit = json.loads(credit_json)
        return credit


class RemittanceCallback(AsyncRequest):
    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        super(RemittanceCallback, self).__init__(gateway_channel_id, schedule_plan_uuid, params)

    def __eval_business_result(self, result):
        super(RemittanceCallback, self).__eval_business_result(result)
        print("__eval_business_result  rpc@contract_callback %s" % self.rpc_channel_id)


class RemittanceService(RemittanceService):
    def __init__(self):
        pass

    def update_remittance_status(self, remittance_application_uuid, remittance_validate_status):
        return self.updateRemittanceStatus(remittance_application_uuid, remittance_validate_status)
    def get_remittance_info(self, merchantContractNo,  vedaContractNo):
        return self.getRemittanceInfo(merchantContractNo,  vedaContractNo)

class AssetValidation(object):

    def __init__(self):
        self.asset_service = AssetService()

    def build_check_list(self, application_data):
        asset_validation = {}

        if not application_data.has_key('merchantContractNo') or application_data['merchantContractNo'] == '':
            asset_validation['is_success'] = False
            message = 'merchantContractNo为空'
            asset_validation['message'] = message
            return asset_validation

        print "商户合同编号是:" , application_data['merchantContractNo']

        contract = self.asset_service.get_contract(application_data['merchantContractNo'])
        print "contract的内容是:" ,contract
        #1合同的存在性
        if contract is None:
            asset_validation['is_success'] = False
            message = '合同不存在'
            asset_validation['message'] = message
            return asset_validation
        #2合同是否成功（成功）
        is_contract_validate_success = contract['contractStatus'] == 'REMITTANCE_SUCCESS'
        print contract['contractStatus']
        if not is_contract_validate_success:
            asset_validation['is_success'] = False
            message = '合同状态不为放款成功'
            asset_validation['message'] = message
            return asset_validation

        product_uuid = application_data['productUuid']
        product_service = ProductService()
        product = product_service.get_product(product_uuid)
        #3产品代码关联性
        is_product_code_correct = product['productCode'] == contract['productCode']
        if not is_product_code_correct:
            asset_validation['is_success'] = False
            message = '产品代码与商户合同编号不匹配'
            asset_validation['message'] = message
            return asset_validation

        # #4放款成功才进行还款计划推送
        # remittance_service = RemittanceService()
        # remittance_info_result_json = remittance_service.get_remittance_info(application_data['merchantContractNo'],application_data['vedaContractNo'])
        # remittance_info_result = json.loads(remittance_info_result_json)
        # if not remittance_info_result['status']:
        #     asset_validation['is_success'] = False
        #     message = '该还款计划对应的放款申请不是成功状态'
        #     asset_validation['message'] = message
        #     return asset_validation
        start_date = product['startDate']
        end_date = product['endDate']
        version_no = application_data['versionNo']
        asset_set_list_json = self.asset_service.get_asset_sets_json(version_no)
        asset_set_list = json.loads(asset_set_list_json)
        print "asset_set_list",asset_set_list
        date_time = '1970-01-01'
        start_time = '1970-01-01'
        principal_sum = contract['principalSum']
        print "合同本金总计为：{}",principal_sum
        asset_set_apply_amount = 0.0
        period_no = 0
        if not asset_set_list is None :
            start_time = asset_set_list[0]['plannedRepaymentDate']
            for asset_set in asset_set_list:
                period_no = period_no + 1
                print "asset_set['repaymentPrincipal']:{}",asset_set['repaymentPrincipal']
                asset_set_apply_amount = asset_set_apply_amount + asset_set['repaymentPrincipal']
                if not date_time_compare(asset_set['plannedRepaymentDate'],date_time):
                    date_time = asset_set['plannedRepaymentDate']
        print "date_time----",date_time
        print "还款计划中本金总计为：{}",asset_set_apply_amount
        effectiveDate = contract['effectiveDate']
        expirationDate = contract['expirationDate']

        #4还款日期不得晚于信托结束时间
        if  time_compare(date_time , end_date):
            asset_validation['is_success'] = False
            message = '最后一期还款计划不得晚于信托结束时间'
            asset_validation['message'] = message
            return asset_validation

        print date_time
        print start_time
        print effectiveDate
        print expirationDate
        effectiveDate = dateToTime(effectiveDate,0)
        expirationDate = dateToTime(expirationDate,0)
        date_time = dateToTime(date_time,1)
        start_time = dateToTime(start_time,1)

        # 最后一期还款时间不能大于合同到期时间
        if not date_time <= expirationDate:
            asset_validation['is_success'] = False
            message = '最后一期还款时间不能大于合同到期时间'
            asset_validation['message'] = message
            return asset_validation

        # 第一期还款时间不能小于合同开始时间
        if not start_time >= effectiveDate:
            asset_validation['is_success'] = False
            message = '第一期还款时间不能小于合同开始时间'
            asset_validation['message'] = message
            return asset_validation

        #5还款计划本金总计不等于合同本金总计
        print "--------------------------"
        print "合同本金总计为：{}",principal_sum
        print "类型为：",type(principal_sum)
        print "还款计划中本金总计为：{}",asset_set_apply_amount
        print "类型为：",type(asset_set_apply_amount)

        if abs(asset_set_apply_amount - principal_sum)>1.0e-9:
            asset_validation['is_success'] = False
            message = '还款计划本金总计不等于合同本金总计'
            asset_validation['message'] = message
            return asset_validation

        # #6还款计划期数校验
        if period_no != 12:
            asset_validation['is_success'] = False
            message = '还款计划期数必须为12期'
            asset_validation['message'] = message
            return asset_validation

        asset_validation['is_success'] = True
        asset_validation['message'] = ''
        return asset_validation

def time_compare(time1,time2):
    time_one = time.mktime(time.strptime(time1,'%Y-%m-%d'))
    time_two = time2/1000
    return time_one >time_two

def date_time_compare(time1,time2):
    time_one = time.mktime(time.strptime(time1,'%Y-%m-%d'))
    time_two = time.mktime(time.strptime(time2,'%Y-%m-%d'))
    return time_one <time_two

def date_time_comparenew(time1,time2):
    time_one = time1/1000
    time_two = time2/1000
    return time_one <time_two

def asset_application_checklist(application_data):
    validator = AssetValidation()
    check_list = validator.build_check_list(application_data)
    return check_list
def dateToTime(date,clas):
    if(clas==0):
        timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
        timeStamp = int(time.mktime(timeArray))
        print date,"时间转换为：",timeStamp
        return timeStamp
    if(clas==1):
        timeArray = time.strptime(date, "%Y-%m-%d")
        timeStamp = int(time.mktime(timeArray))
        print date,"时间转换为：",timeStamp
        return timeStamp



def extract_business_data(request_data):
    print request_data
    request_content = request_data['requestContent']
    print request_content

    asset_version = json.loads(request_content)
    print "asset_version----",asset_version
    return asset_version


def update_asset_version_validate_status(asset_set_version_uuid, asset_version_validate):
    asset_service = AssetService()
    asset_service.update_asset_version_status(asset_set_version_uuid, asset_version_validate['is_success'],asset_version_validate['message'])


def extract_call_postloan_data(application_data):
    call_postloan_data = {}

    asset_service = AssetService()
    contract = asset_service.get_contract(application_data['merchantContractNo'])
    print("contract的值是==========%s" % contract)

    product_service = ProductService()
    product = product_service.get_product(application_data['productUuid'])

    credit_service = CreditService()
    credit = credit_service.get_credit_application(product['productCode'], application_data['vedaCreditNo'])
    print("credit的值是============%s" % credit)

    call_postloan_data['vedaContractNo'] = application_data['vedaContractNo']
    call_postloan_data['merchantContractNo'] = application_data['merchantContractNo']
    call_postloan_data['vedaCreditNo'] = application_data['vedaCreditNo']

    call_postloan_data['borrowerUuid'] = credit['borrowerUuid']
    call_postloan_data['name'] = credit['name']
    call_postloan_data['certificateNo'] = credit['certificateNo']

    # # bankName、bankcardNo从contract放款信息段取
    # loan_info_list_json = contract['loanInfoSection']
    # loan_info_list = json.loads(loan_info_list_json)
    # loan_info = loan_info_list[0]
    # call_postloan_data['bankName'] = loan_info['payeeBankCode']
    #
    # call_postloan_data['bankcardNo'] = loan_info['payeeBankAccountNo']

    borrower_info_json = credit_service.selectBorrowerInfo(credit['creditApplicationUuid'])
    borrower_info = json.loads(borrower_info_json)
    print "borrower_info,{}",borrower_info
    bank_card_section = borrower_info['bankcardInfoSection']
    bank_card_info = json.loads(bank_card_section)
    if bank_card_info.has_key('bankcardBindingMobile')and bank_card_info['bankcardBindingMobile']:
        call_postloan_data['mobile'] = bank_card_info['bankcardBindingMobile']
    else:
        call_postloan_data['mobile'] = credit['mobile']
    call_postloan_data['bankName'] = bank_card_info['bankName']
    call_postloan_data['bankcardNo'] = bank_card_info['bankcardNo']
    call_postloan_data['principalSum'] = contract['principalSum']
    call_postloan_data['valueDate'] = contract['valueDate']
    call_postloan_data['expirationDate'] = contract['expirationDate']
    call_postloan_data['interestRate'] = contract['interestRate']
    call_postloan_data['interestRateCycle'] = contract['interestRateCycle']
    call_postloan_data['repaymentMode'] = contract['repaymentMode']
    call_postloan_data['productCode'] = product['productCode']

    asset_set_list = asset_service.get_asset_sets_json(application_data['versionNo'])
    call_postloan_data['assetSet'] = asset_set_list

    return call_postloan_data

def call_back_data(application_data, product_code, asset_version_validate_status,call_back_url, remark):
    call_back_parameter = {}
    call_back_parameter['callbackUrl'] = call_back_url
    call_back_parameter['productCode'] = product_code
    call_back_parameter['merchantContractNo'] = application_data['merchantContractNo']
    call_back_parameter['vedaContractNo'] = application_data['vedaContractNo']
    call_back_parameter['vedaCreditNo'] = application_data['vedaCreditNo']
    call_back_parameter['remark'] = remark
    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    call_back_parameter['gmtModified'] = time
    if asset_version_validate_status:
        call_back_parameter['status'] = 2
    else:
        call_back_parameter['status'] = 3
    print "先花还款计划回调参数：",call_back_parameter
    return call_back_parameter

#单独回调
def call_back(application_data, schedule_plan_uuid, product_code, asset_version_validate_status,call_back_url, remark):
    print "开始回调"
    # call_back_parameter = call_back_data(application_data, product_code, asset_version_validate_status,call_back_url, remark.decode('utf-8'))
    # rpc_remittance_callback_obj = RemittanceCallback("wolaidai_CALL_BACK_ASSET_SET", schedule_plan_uuid,
    #                                                  call_back_parameter)
    # rpc_remittance_callback_obj.emit()
    print "回调结束"


def get_contract_from_loan_contract(merchant_contract_no):
    asset_service = AssetService()
    contract = asset_service.get_contract(merchant_contract_no)
    return contract

def update_contract_status(veda_contract_no,status,remark):
    asset_service = AssetService()
    asset_service.update_contract_status(veda_contract_no,status,remark)

def get_product(application_data):
    product_service = ProductService()
    product = product_service.get_product(application_data['productUuid'])
    return product
def asset_validate(request_data):
    application_data = extract_business_data(request_data)
    schedule_plan_uuid = request_data['schedulePlanUuid']
    product_uuid = application_data['productUuid']
    product_service = ProductService()
    product = product_service.get_product(product_uuid)

    call_back_url = None
    if application_data.has_key('callbackUrl') and application_data['callbackUrl']:
        call_back_url = application_data['callbackUrl']
    print("application_data的内容是%s" % application_data)
    # 1、还款计划业务校验
    asset_validation = asset_application_checklist(application_data)
    print("还款计划业务校验结束，校验的结果是%s" % asset_validation)
    # 2.更新还款计划状态
    update_asset_version_validate_status(application_data['assetSetVersionUuid'], asset_validation)
    if call_back_url != None:
        call_back(application_data,schedule_plan_uuid,product['productCode'], asset_validation['is_success'],call_back_url, asset_validation['message'])
    if asset_validation['is_success']:
        pass
    else:
        print "还款计划校验失败，【assetSetVersionUuid】为：{}，【remark】为：{}".format(application_data['assetSetVersionUuid'],asset_validation['message'])
        return "end"

def run_script(request_data):

    application_data = extract_business_data(request_data)
    print("application_data的内容是%s" % application_data)
    schedule_plan_uuid = request_data['schedulePlanUuid']
    product_uuid = application_data['productUuid']
    product_service = ProductService()
    product = product_service.get_product(product_uuid)

    call_back_url = None
    if application_data.has_key('callbackUrl') and application_data['callbackUrl']:
        call_back_url = application_data['callbackUrl']
    print("application_data的内容是%s" % application_data)
    # 1、还款计划业务校验
    asset_validation = asset_application_checklist(application_data)
    print("还款计划业务校验结束，校验的结果是%s" % asset_validation)
    # 2.更新还款计划状态
    update_asset_version_validate_status(application_data['assetSetVersionUuid'], asset_validation)
    # if call_back_url != None:
    call_back(application_data,schedule_plan_uuid,product['productCode'], asset_validation['is_success'],call_back_url, asset_validation['message'])
    if asset_validation['is_success']:
        pass
    else:
        return

    # 3、资产包导入贷后extract_call_postloan_data
    call_postloan_data = extract_call_postloan_data(application_data)
    print("call_postloan_data----->%s" % call_postloan_data)
    rpc_asset_set_obj = AssetSetPostloanCall("wolaidai_ASSET_SET_POSTLOAN", schedule_plan_uuid, call_postloan_data)
    yield rpc_asset_set_obj.call()
    print "还款计划导入贷后完成"
    if rpc_asset_set_obj.response().rpc_business_state == RESPONSE_BUSINESS_OK:
        #成功后更新合同状态为已生效
        update_contract_status(application_data['vedaContractNo'],9,None)

def create_engin(request_data):
    print("西藏我来贷还款计划开始...")
    # asset_validate_result = asset_validate(request_data)
    # if asset_validate_result == "end":
    #     print("西藏先花还款计划结束(业务校验失败)")
    #     return "end"
    runtime = run_script(request_data)
    engine = AsyncEngine(runtime)
    result = engine.one_step()
    if "end" != result:
        return engine
    else:
        print("西藏我来贷还款计划结束")
        return result
