# -*- coding: UTF-8 -*-
import json
import os
import types
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService, AssetService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'


class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class SealCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(SealCall, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(SealCall, self).__eval_business_result(result)
        print "__eval_business_result  rpc@cfca_seal_call %s" % self.rpc_channel_id
        pass


class FileUpload(AsyncRequest):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(FileUpload, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(FileUpload, self).__eval_business_result(result)
        print "__eval_business_result  rpc@FileUpload %s" % self.rpc_channel_id
        pass


class AssetService(AssetService):
    def __init__(self):
        pass

    def update_contract_status(self, contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                               contract_external_no, remark):
        return self.updateContractStatusAndRemark(contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                                                  contract_external_no, remark)

    def get_contract(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def update_file_status(self, file_uuid, file_status, sealed_file_address):
        return self.updateFileStatus(file_uuid, file_status, sealed_file_address)


class CreditService(CreditService):
    def __init__(self):
        pass

    def get_credit_application_by_merchant_creditNo(self, product_code, veda_credit_no):
        credit_json = self.getCreditApplicationByMerchantCreditNo(product_code, veda_credit_no)
        credit = json.loads(credit_json)
        return credit

    def get_credit_application_by_veda_creditNo(self, product_code, merchant_credit_no):
        credit_json = self.getCreditApplicationByVedaCreditNo(product_code, merchant_credit_no)
        credit = json.loads(credit_json)
        return credit


class FileUploadCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(FileUploadCall, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(FileUploadCall, self).__eval_business_result(result)
        print "__eval_business_result  rpc@file_upload_call %s" % self.rpc_channel_id
        pass

def check_file_type(application_data):
    file_type_list = []
    for file_info in application_data:
        file_type = file_info['fileType']
        file_type_list.append(file_type)
    if '3' in file_type_list:
        return True
    return False

def update_seal_status(file_uuid, seal_status, sealed_file_address):
    asset_service = AssetService()
    asset_service.update_file_status(file_uuid, seal_status, sealed_file_address)


def update_seal_validate_status(file_uuid, seal_validate_status, file_address):
    asset_service = AssetService()
    if not seal_validate_status:
        asset_service.update_file_status(file_uuid, 3, file_address)


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    file_info = json.loads(request_content)
    return file_info


def file_validate_data(file_info):
    validate_result = True

    credit_service = CreditService()
    asset_service = AssetService()

    if not (file_info.has_key('merchantCreditNo') and file_info['merchantCreditNo'] != ""):
        validate_result = False
        return validate_result

    credit = credit_service.get_credit_application_by_veda_creditNo(file_info['productCode'],
                                                                    file_info['merchantCreditNo'])

    print("credit is %s" % credit)
    if credit is None:
        validate_result = False
        return validate_result

    if not (file_info.has_key('merchantCreditNo') and file_info['merchantCreditNo'] != ""):
        validate_result = False
        return validate_result

    contract = asset_service.get_contract(file_info['merchantContractNo'])
    print("contract is %s " % contract)

    if contract is None:
        validate_result = False
        return validate_result

    if file_info['fileType'] != '3':
        validate_result = False
        return validate_result

    return validate_result


def make_dir(file_path):
    is_exists = os.path.exists(file_path)
    if not is_exists:
        os.makedirs(file_path)
    return file_path


def extract_call_cfca_data(application_data):
    call_cfca_data = {}

    call_cfca_data['contractFilePath'] = application_data['fileAddress']

    file_path = application_data['fileAddress']
    file_name = os.path.basename(file_path)
    file_name_after_seal = file_name.split('.')[0] + '_sealed.' + file_name.split('.')[1]

    call_cfca_data['contractFilePathAfterStamped'] = os.path.dirname(file_path) + '/' + file_name_after_seal

    return call_cfca_data


def build_file_upload_data(file_path, product_code):
    file_upload_data = {}
    print "-------------------------------------"
    file_upload_data['fileType'] = "sftp"
    today = datetime.datetime.now().strftime('%Y%m%d')
    # 目标地址
    file_upload_data['uploadPath'] = "/Download/" + product_code + "/" + today + "/Assetattachment"
    file_name = file_path.split("/")[-1]
    file_upload_data['uploadFileName'] = file_name
    print "file_name"
    print file_name
    file_upload_data['localPath'] = file_path
    print "-------------------------------------"
    return file_upload_data


def run_script(request_data):
    application_data = extract_business_data(request_data)
    print("application的值是%s" % application_data)
    schedule_plan_uuid = request_data['schedulePlanUuid']

    # 遍历附件
    for file_info in application_data:
        print("打印出fileinfo的值是%s" % file_info)
        validate_result = file_validate_data(file_info)
        print("validate_result is %s " % validate_result)
        if not validate_result:
            continue

        # cfca
        call_cfca_data = extract_call_cfca_data(file_info)
        print("cfca签章参数是%s" % call_cfca_data)
        cfca_seal_call_req = SealCall("YQG_COMMON_SEAL_CREATE", schedule_plan_uuid, call_cfca_data)
        yield cfca_seal_call_req.call()

        if cfca_seal_call_req.response().rpc_business_state == RESPONSE_BUSINESS_FAILED:
            update_seal_status(file_info['fileUuid'], 6, None)
            continue

        if cfca_seal_call_req.response().rpc_business_state == RESPONSE_BUSINESS_OK:
            sealedContractSavePath = call_cfca_data['contractFilePathAfterStamped']
            update_seal_status(file_info['fileUuid'], 5, sealedContractSavePath)
            # upload
            file_upload_data = build_file_upload_data(sealedContractSavePath, file_info['productCode'])
            upload_file_req = FileUpload("YQG_FILE_UPLOAD", schedule_plan_uuid, file_upload_data)
            yield upload_file_req.call()
            if upload_file_req.response().rpc_business_state == RESPONSE_BUSINESS_OK:
                print "file upload success"
            else:
                print "file upload failed"




def create_engin(request_data):
    application_data = extract_business_data(request_data)
    check_result = check_file_type(application_data)
    print("check_result is %s " % check_result)
    if check_result:
        runtime = run_script(request_data)
        engine = AsyncEngine(runtime)
        result = engine.one_step()
        if "end" != result:
            return engine
        else:
            return result
    else:
        return "end"
