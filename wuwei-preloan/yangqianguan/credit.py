# coding=utf-8
import time
import json
import types
import re
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService
from com.suidifu.preloan.scheduler.service import SummaryService
from com.suidifu.preloan.scheduler.service import AssetService
from com.suidifu.preloan.scheduler.service import CreditLimitService

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012


class async_engine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sendingMsg):
        if len(self.stack) == 0:
            return "end"
        msgToSend = sendingMsg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msgToSend is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msgToSend)
                    msgToSend = None
                if isinstance(ret, types.GeneratorType):
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sendingMsg):
        return self.__one_step(sendingMsg)

    def OneStep(self, ):
        return self.__one_step(None)


class async_response:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_commnucation_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


# 通过身份证号获取年龄
def get_age_by_idcard_num(id_card):
    if len(id_card) == 18:
        id_birth_year = int(id_card[6:10])
        id_birth_month = int(id_card[10:12])
        id_birth_day = int(id_card[12:14])
    # 15位身份证情况
    elif len(id_card) == 15:
        id_birth_year_str = id_card[6:8]
        id_birth_month = int(id_card[8:10])
        id_birth_day = int(id_card[10:12])
        id_birth_year = int('19' + id_birth_year_str)
    else:
        return 0

    now = datetime.datetime.now()
    year = now.year
    month = now.month
    day = now.day

    if year == id_birth_year:
        return 0
    else:
        print("id_birth_month is %s" % id_birth_month)
        print("month is %s" % month)
        print("id_birth_day is %s" % id_birth_day)
        print("day is %s" % day)
        if (id_birth_month > month) or (id_birth_month == month and id_birth_day > day):
            return year - id_birth_year - 1
        else:
            return year - id_birth_year

class async_request(object):

    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedulePlanUuid = schedulePlanUuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print "*response* -------->"
        print response
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = async_response(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                      response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if 'FAILED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif 'SUCCEED' == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield

            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        scheduleItemHandler = schedule_item_handler()
        request_uuid = scheduleItemHandler.send_item_rpc(self.rpc_channel_id, self.schedulePlanUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class schedule_item_handler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedulePlanUuid, rpc_params):
        print rpc_channel_id
        print rpc_params
        print type(rpc_params)
        print rpc_params.has_key('schedulePlanUuid')
        return self.sendItemRpc(rpc_channel_id, schedulePlanUuid, rpc_params)


class credit_service(CreditService):
    def __init__(self):
        pass

    def create_borrower(self, application_data):
        return self.createBorrower(application_data['certificateType'], application_data['certificateNo'],
                                   application_data['name'])

    def update_credit_status(self, credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                             external_customer_no_section, remark):
        return self.updateCreditStatus(credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                                       external_customer_no_section, remark)


class asset_service(AssetService):
    def __init__(self):
        pass

    def create_loan_contract(self, summary_asset_uuid, veda_credit_no):
        result_json = self.createLoanContractApplicationFromCredit(summary_asset_uuid, veda_credit_no)
        result = json.loads(result_json)
        return result


class summary_service(SummaryService):
    def __init__(self):
        pass

    def update_summary_status(self, summary_asset_uuid, summary_status, remark):
        return self.updateSummaryAssetStatus(summary_asset_uuid, summary_status, remark)

    def get_summary_by_uuid(self, summary_asset_uuid):
        summary_json = self.getSummaryAsset(summary_asset_uuid)
        return json.loads(summary_json)


class summary_call_back(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(summary_call_back, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(summary_call_back, self).__eval_business_result(result)
        print "__eval_business_result  rpc@summary_call_back %s" % self.rpc_channel_id
        pass


class tongdun_credit_guard(async_request):
    def __init__(self, gateway_channel_id, schedulePlanUuid, params):
        super(tongdun_credit_guard, self).__init__(gateway_channel_id, schedulePlanUuid, params)
        pass

    def __eval_business_result(self, result):
        super(tongdun_credit_guard, self).__eval_business_result(result)
        print "__eval_business_result  rpc@tongdun_credit_guard %s" % self.rpc_channel_id
        pass


def extract_business_data(request_data):
    request_content = request_data['requestContent']
    summary_application = json.loads(request_content)
    return summary_application


def checkIdcard(idcard):
    result = {}
    Errors = ['验证通过!', '身份证号码位数不对!', '身份证号码出生日期超出范围或含有非法字符!', '身份证号码校验错误!', '身份证地区非法!']
    area = {"11": "北京", "12": "天津", "13": "河北", "14": "山西", "15": "内蒙古", "21": "辽宁", "22": "吉林", "23": "黑龙江",
            "31": "上海",
            "32": "江苏", "33": "浙江", "34": "安徽", "35": "福建", "36": "江西", "37": "山东", "41": "河南", "42": "湖北", "43": "湖南",
            "44": "广东", "45": "广西", "46": "海南", "50": "重庆", "51": "四川", "52": "贵州", "53": "云南", "54": "西藏", "61": "陕西",
            "62": "甘肃", "63": "青海", "64": "宁夏", "65": "新疆", "71": "台湾", "81": "香港", "82": "澳门", "91": "国外"}
    idcard = str(idcard)
    idcard = idcard.strip()
    idcard_list = list(idcard)

    # 地区校验
    key = idcard[0: 2]
    if key in area.keys():
        if not area[idcard[0:2]]:
            result['result'] = False
            result['msg'] = Errors[4]
            return result
    else:
        result['result'] = False
        result['msg'] = Errors[4]
        return result
    # 15位身份号码检测

    if len(idcard) == 15:
        if ((int(idcard[6:8]) + 1900) % 4 == 0 or (
                (int(idcard[6:8]) + 1900) % 100 == 0 and (int(idcard[6:8]) + 1900) % 4 == 0)):
            ereg = re.compile(
                '[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}$')  # //测试出生日期的合法性
        else:
            ereg = re.compile(
                '[1-9][0-9]{5}[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}$')  # //测试出生日期的合法性
        if re.match(ereg, idcard):
            result['result'] = True
            result['msg'] = Errors[0]
            return result
        else:
            result['result'] = False
            result['msg'] = Errors[2]
            return result
    # 18位身份号码检测
    elif len(idcard) == 18:
        # 出生日期的合法性检查
        # 闰年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))
        # 平年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))
        if int(idcard[6:10]) % 4 == 0 or (int(idcard[6:10]) % 100 == 0 and int(idcard[6:10]) % 4 == 0):
            ereg = re.compile(
                '[1-9][0-9]{5}(19[0-9]{2}|20[0-9]{2})((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9Xx]$')  # //闰年出生日期的合法性正则表达式
        else:
            ereg = re.compile(
                '[1-9][0-9]{5}(19[0-9]{2}|20[0-9]{2})((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9Xx]$')  # //平年出生日期的合法性正则表达式
        # //测试出生日期的合法性
        if re.match(ereg, idcard):
            # //计算校验位
            S = (int(idcard_list[0]) + int(idcard_list[10])) * 7 + (int(idcard_list[1]) + int(idcard_list[11])) * 9 + (
                    int(
                        idcard_list[2]) + int(idcard_list[12])) * 10 + (
                        int(idcard_list[3]) + int(idcard_list[13])) * 5 + (int(
                idcard_list[4]) + int(idcard_list[14])) * 8 + (int(idcard_list[5]) + int(idcard_list[15])) * 4 + (int(
                idcard_list[6]) + int(idcard_list[16])) * 2 + int(idcard_list[7]) * 1 + int(idcard_list[8]) * 6 + int(
                idcard_list[9]) * 3
            Y = S % 11
            M = "F"
            JYM = "10X98765432"
            M = JYM[Y]  # 判断校验位
            if M == idcard_list[17]:  # 检测ID的校验位
                result['result'] = True
                result['msg'] = Errors[0]
                return result
            else:
                result['result'] = False
                result['msg'] = Errors[3]
                return result
        else:
            result['result'] = False
            result['msg'] = Errors[2]
            return result
    else:
        result['result'] = False
        result['msg'] = Errors[1]
        return result


# 校验数据
def validate_data(application_data):
    validate_result = {}
    # 校验姓名不能为空
    if (not application_data.has_key('name')) or (not application_data['name']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，姓名为空'
        return validate_result

    # 校验证件类型不能为空（新增）
    if (not application_data.has_key('certificateType')) or (not application_data['certificateType']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，证件类型为空'
        return validate_result

    # 校验出生日期不能为空（新增）
    if (not application_data.has_key('birthDate')) or (not application_data['birthDate']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，出生日期为空'
        return validate_result

    # 校验手机号码不能为空（新增）
    if (not application_data.has_key('mobile')) or (not application_data['mobile']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，联系方式为空'
        return validate_result

    if(not application_data.has_key('applyAmount')) or (not application_data['applyAmount']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，申请额度不能为空'
        return validate_result

    # 校验证件号不能为空
    if (not application_data.has_key('certificateNo')) or (not application_data['certificateNo']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，证件号码为空'
        return validate_result

    check_id_card = checkIdcard(application_data['certificateNo'])
    if not check_id_card['result']:
        validate_result['status'] = False
        validate_result['message'] = check_id_card['msg']
        return validate_result

    # 校验还款账户信息（新增）
    # 还款银行卡账号不能为空
    borrower_json = application_data['borrowerApplicationInfo']
    bank_card_info = borrower_json['bankcardInfoSection']
    bank_card_info_json = json.loads(bank_card_info)
    if(not bank_card_info_json.has_key('bankcardNo')) or (not bank_card_info_json['bankcardNo']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，还款银行卡账号为空'
        return validate_result

    if (not bank_card_info_json.has_key('bankName')) or (not bank_card_info_json['bankName']):
        validate_result['status'] = False
        validate_result['message'] = '授信失败，开户银行为空'
        return validate_result

    # 校验年龄
    # attached_json = application_data['attachedContentSection']
    # attached = json.loads(attached_json)
    # user_age = attached['userAge']
    age = get_age_by_idcard_num(application_data['certificateNo'])
    # user_age < '22' or user_age > '55'
    if age < 22:
        validate_result['status'] = False
        validate_result['message'] = '授信失败，年龄需大于22周岁'
        return validate_result

    validate_result['status'] = True
    validate_result['message'] = ''
    return validate_result


def create_borrower(application_data):
    creditService = credit_service()
    return creditService.create_borrower(application_data)


def update_credit_status(credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                         external_customer_no_section, remark):
    creditService = credit_service()
    creditService.update_credit_status(credit_application_uuid, credit_status_ordinal, credit_line, borrower_uuid,
                                       external_customer_no_section, remark)


def create_contract(summary_asset_uuid, veda_credit_no):
    assetService = asset_service()
    return assetService.create_loan_contract(summary_asset_uuid, veda_credit_no)


def update_summary(summary_asset_uuid, summary_status, remark):
    summaryService = summary_service()
    summaryService.update_summary_status(summary_asset_uuid, summary_status, remark)


def get_summary_asset(summary_asset_uuid):
    summaryService = summary_service()
    return summaryService.get_summary_by_uuid(summary_asset_uuid)


def build_bairong_param(application_data):
    param = {'name': application_data['name'],
             'id': application_data['certificateNo'],
             'cell': application_data['mobile'],
             'strategyId': 'DTA_BR0000161'}
    return param


def build_callback_data(summary_asset, result):
    callback_data = {'applicationNo': summary_asset['summaryNo'],
                     'auditDate': time.strftime("%Y%m%d%H%M%S"),
                     'auditResult': '02',
                     'auditRejectReason': result['message']}
    return callback_data


def build_callback_data_by_validate(summary_asset, validate_result):
    callback_data = {'applicationNo': summary_asset['summaryNo'],
                     'auditDate': time.strftime("%Y%m%d%H%M%S")}
    if validate_result['status']:
        callback_data['auditResult'] = '01'
    else:
        callback_data['auditResult'] = '02'
    callback_data['auditRejectReason'] = validate_result['message']
    return callback_data


def run_script(request_data):
    print "start [run_script] -------->"
    print "[run_script] success *request_data* -------->"
    print request_data
    print "start [extract_business_data] -------->"
    application_data = extract_business_data(request_data)
    print "[extract_business_data] success *application_data* -------->"
    print application_data
    scheduler_plan_uuid = request_data['schedulePlanUuid']
    # 校验授信信息
    print "start [validate_data] -------->"
    validate_result = validate_data(application_data)
    print "[validate_data] success *validate_result* -------->"
    print validate_result
    # 创建借款人信息
    print "start [create_borrower] -------->"
    borrower_uuid = create_borrower(application_data)
    print "[create_borrower] success *borrower_uuid* -------->"
    print borrower_uuid
    credit_application_uuid = application_data['creditApplicationUuid']
    # 百融校验
    if validate_result['status']:
        tongdun_param = build_bairong_param(application_data)
        rpc_td_req = tongdun_credit_guard("YQG_BAIRONG_CREDIT", scheduler_plan_uuid, tongdun_param)
        yield rpc_td_req.call()
        # rpc_td_req.emit()
        td_result_json = rpc_td_req.response().rpc_result
        print td_result_json
        # td_result = json.loads(td_result_json)
        # td_result_json = td_result['analysed']
        # td_result = json.loads(td_result_json)
        # td_msg = '风控拒绝' + ',' + td_result['finalDecision'] + "," + td_result['finalScore'] + "," + td_result['riskName']
        # td_msg = td_msg.encode("ISO-8859-1")
        # if rpc_td_req.response().rpc_business_state == RESPONSE_BUSINESS_OK:
        #     validate_result['status'] = True
        #     validate_result['message'] = td_msg
        # else:
        #     validate_result['status'] = False
        #     validate_result['message'] = td_msg

    # 创建用信或更新状态
    summary_asset_uuid = application_data['summaryAssetUuid']
    summary_asset = get_summary_asset(summary_asset_uuid)
    if validate_result['status']:
        credit_line = application_data['applyAmount']
        print "start [update_credit_status] -------->"
        update_credit_status(credit_application_uuid, 2, credit_line, borrower_uuid, None, validate_result['message'])
        # 创建借款人信息
        credit_limit_service = CreditLimitService()
        borrower_account_string = credit_limit_service.adjustEvent(borrower_uuid, application_data['productCode'], application_data['vedaCreditNo'], 'CREDIT')
        borrower_account = json.loads(borrower_account_string)
        if borrower_account['code'] != '0000':
            # 授信失败，更新授信信息、资产进件信息，回调结果
            update_credit_status(credit_application_uuid, 3, 0.00, borrower_uuid, None, '创建借款人账户信息失败')
            return
        # 创建用信
        print "start [create_contract] -------->"
        result = create_contract(summary_asset_uuid, application_data['vedaCreditNo'])
        print "[create_contract] success *result* -------->"
        print result
        # 创建用信失败失败
        if result['code'] != '0000':
            print "start [update_summary] -------->"
            update_summary(summary_asset_uuid, 2, result['message'])
            print "[update_summary] success -------->"
            # 创建用信失败进行回调
            # print "start [summary_call_back] -------->"
            # callback_data = build_callback_data(summary_asset, result)
            # callback_req = summary_call_back("JD_JD_SUMMARY_CALLBACK", scheduler_plan_uuid, callback_data)
            # callback_req.emit()
            # print "[summary_call_back] success -------->"
            return
        update_credit_status(credit_application_uuid, 2, credit_line, borrower_uuid, None,validate_result['message'])
    else:
        print "start [update_credit_status] -------->"
        update_credit_status(credit_application_uuid, 3, 0.00, borrower_uuid, None, validate_result['message'])
        update_summary(summary_asset_uuid, 1, validate_result['message'])
        # print "start [summary_call_back] -------->"
        # callback_data = build_callback_data_by_validate(summary_asset, validate_result)
        # callback_req = summary_call_back("JD_JD_SUMMARY_CALLBACK", scheduler_plan_uuid, callback_data)
        # callback_req.emit()
        # print "[summary_call_back] success -------->"
    print "[update_credit_status] success -------->"


def create_engin(request_data):
    try:
        runtime = run_script(request_data)
        engine = async_engine(runtime)
        result = engine.OneStep()
        if "end" != result:
            return engine
        else:
            return result
    except Exception, e:
        print e
    finally:
        pass
