# -*- coding: UTF-8 -*-
import json
import types
import time
import datetime
from com.suidifu.preloan.scheduler.handler.impl import ScheduleItemHandlerImpl
from com.suidifu.preloan.scheduler.service import CreditService, SummaryService, AssetService, ProductService, \
    RemittanceService
from java.lang import Exception

REQUEST_PENDING = 0
REQUEST_PROCESSING = 1
REQUEST_DONE = 2
REQUEST_TIMEOUT = 3
RESPONSE_SERVICE_HTTP_404 = 404
RESPONSE_SERVICE_HTTP_502 = 502
RESPONSE_SERVICE_REFUSE_TO_CONNECT = 0x0001
RESPONSE_SERVICE_NONE = 0x0002
RESPONSE_SERVICE_OK = 200
RESPONSE_BUSINESS_OK = 0x0010
RESPONSE_BUSINESS_FAILED = 0x0011
RESPONSE_BUSINESS_NONE = 0x0012
RESPONSE_SUCCESS_FLAG = 'SUCCEED'
RESPONSE_FAILED_FLAG = 'FAILED'

import sys
print("the default encoding:",sys.getdefaultencoding())

class AsyncEngine:
    def __init__(self, runtime):
        self.runtime = runtime
        self.stack = []
        self.stack.append(runtime)

    def __one_step(self, sending_msg):
        if len(self.stack) == 0:
            return "end"
        msg_to_send = sending_msg
        while len(self.stack) != 0:
            self.runtime = self.stack[-1]
            try:
                if msg_to_send is None:
                    ret = self.runtime.next()
                else:
                    ret = self.runtime.send(msg_to_send)
                    msg_to_send = None
                if isinstance(ret, types.GeneratorType) is True:
                    self.stack.append(ret)
                else:
                    break
            except StopIteration as _e:
                self.stack.pop()

    def Send(self, sending_msg):
        return self.__one_step(sending_msg)

    def one_step(self):
        return self.__one_step(None)


class AsyncResponse:
    def __init__(self, comm_result, rpc_result, req_id):
        self.rpc_communication_result = comm_result
        self.rpc_result = rpc_result
        self.rpc_business_state = RESPONSE_BUSINESS_NONE
        self.rpc_remote_request_id = req_id

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class AsyncRequest(object):

    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        self.rpc_channel_id = gateway_channel_id
        self.schedule_planUuid = schedule_plan_uuid
        self.rpc_params = params
        self.rpc_processing_state = REQUEST_PENDING
        self.rpc_response = None
        self.rpc_remote_request_id = None

    def eval_rpc_response(self, response):
        print("response===========结果是%s" % response)
        self.rpc_processing_state = REQUEST_DONE
        response_dic = json.loads(response)

        response_obj = AsyncResponse(RESPONSE_SERVICE_OK, response_dic['resultContent'],
                                     response_dic['schedulePlanUuid'])
        self.rpc_response = response_obj

        if RESPONSE_FAILED_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_FAILED
        elif RESPONSE_SUCCESS_FLAG == response_dic['businessStatus']:
            self.rpc_response.rpc_business_state = RESPONSE_BUSINESS_OK

    def emit(self):
        if self.rpc_processing_state != REQUEST_PENDING:
            print("rpc_processing_state  %d pass" % self.rpc_processing_state)
        self.rpc_remote_request_id = self.__send_rpc()
        self.rpc_processing_state = REQUEST_PROCESSING

    def collect(self):
        while self.rpc_processing_state == REQUEST_PROCESSING:
            print("%s is collecting result" % self.rpc_channel_id)
            result = yield
            print(self.rpc_channel_id + " response is ready")
            self.eval_rpc_response(result)

    def call(self):
        self.emit()
        yield self.collect()

    def __send_rpc(self):
        schedule_item_handler = ScheduleItemHandler()
        request_uuid = schedule_item_handler.send_item_rpc(self.rpc_channel_id, self.schedule_planUuid, self.rpc_params)
        print("emit " + self.rpc_channel_id)
        return request_uuid

    def response(self):
        return self.rpc_response

    def get_rpc_request_id(self):
        return self.rpc_remote_request_id


class ScheduleItemHandler(ScheduleItemHandlerImpl):
    def __init__(self):
        pass

    def send_item_rpc(self, rpc_channel_id, schedule_plan_uuid, rpc_params):
        print("rpc_channel_id的内容是%s" % rpc_channel_id)
        print("rpc_params的内容是%s" % rpc_params)
        return self.sendItemRpc(rpc_channel_id, schedule_plan_uuid, rpc_params)


class AssetService(AssetService):
    def __init__(self):
        pass

    def get_contract(self, merchant_contract_no):
        contract_json = self.selectLoanContractByMerchantContractNo(merchant_contract_no)
        contract = json.loads(contract_json)
        return contract

    def update_contract_status(self, contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                               contract_external_no, remark):
        return self.updateContractStatusAndRemark(contract_uuid, contract_status_ordinal, veda_credit_no, principal_sum,
                                                  contract_external_no, remark)


    def update_contract_date(self, merchant_contract_no):
        new_date = time.strftime('%Y-%m-%d', time.localtime(time.time()))
        self.updateContractStartDate(merchant_contract_no, new_date)


class RemittanceService(RemittanceService):
    def __init__(self):
        pass

    def update_remittance_status(self, remittance_application_uuid, remittance_validate_status, remark):
        return self.updateRemittanceStatus(remittance_application_uuid, remittance_validate_status, remark)


class Callback(AsyncRequest):
    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        super(Callback, self).__init__(gateway_channel_id, schedule_plan_uuid, params)
        pass

    def __eval_business_result(self, result):
        super(Callback, self).__eval_business_result(result)
        print("__eval_business_result  rpc@CreditCallback %s" % self.rpc_channel_id)
        pass


# 调用贷后
class RemittancePostloanCall(AsyncRequest):
    def __init__(self, gateway_channel_id, schedule_plan_uuid, params):
        super(RemittancePostloanCall, self).__init__(gateway_channel_id, schedule_plan_uuid, params)
        pass

    def __eval_business_result(self, result):
        super(RemittancePostloanCall, self).__eval_business_result(result)
        print("result的值是================%s" % result)
        print("__eval_business_result  rpc@remittance_postloan_call %s" % self.rpc_channel_id)


# 组装调用贷后参数
def extract_call_postloan_data(application_data):
    call_postloan_data = {}
    call_postloan_data['merchantLoanOrderNo'] = application_data['merchantLoanOrderNo']
    call_postloan_data['productCode'] = application_data['productCode']

    asset_service = AssetService()
    contract = asset_service.get_contract(application_data['merchantContractNo'])

    call_postloan_data['merchantLoanOrderNo'] = application_data['merchantLoanOrderNo']
    call_postloan_data['productCode'] = application_data['productCode']
    call_postloan_data['vedaContractNo'] = contract['vedaContractNo']
    call_postloan_data['merchantContractNo'] = application_data['merchantContractNo']
    call_postloan_data['principalSum'] = application_data['loanAmount']
    call_postloan_data['loanAmount'] = application_data['loanAmount']
    call_postloan_data['loanStrategy'] = 0
    call_postloan_data['loanDetail'] = application_data['remittanceApplicationDetails']
    return call_postloan_data


def extract_application_data(request_data):
    request_content = request_data['requestContent']
    application = json.loads(request_content)
    return application


def remittance_validate_data(application_data):
    asset_service = AssetService()

    # 通过商户合同编码获取合同信息
    print("参数是%s" % application_data['merchantContractNo'])
    contract = asset_service.get_contract(application_data['merchantContractNo'])
    print("contract的内容是========%s" % contract)

    remittance_validate_result = {}
    if contract is None:
        remittance_validate_result['status'] = False
        remittance_validate_result['message'] = '找不到对应的用信'
        return remittance_validate_result

    is_contract_validate_success = (contract['contractStatus'] == 'AUDIT_SUCCEED' or contract['contractStatus'] =='REMITTANCE_FAILED')
    if not is_contract_validate_success:
        remittance_validate_result['status'] = False
        remittance_validate_result['message'] = '用信不处于可放款状态'
        return remittance_validate_result

    # 放款各明细的总额
    sum_amount = 0

    remittance_application_list = application_data['remittanceApplicationDetails']

    for remittance in remittance_application_list:
        sum_amount += remittance['loanAmount']

    print("放款明细总额是==========%s" % sum_amount)

    is_loan_amount_equal_detail = application_data['loanAmount'] == sum_amount
    if not is_loan_amount_equal_detail:
        remittance_validate_result['status'] = False
        remittance_validate_result['message'] = '本次放款总额不等于各放款明细之和'
        return remittance_validate_result

    print("合同本金总计是=============%s" % contract['principalSum'])
    is_remittance_sum_check = application_data['loanAmount'] <= contract['principalSum']

    if not is_remittance_sum_check:
        remittance_validate_result['status'] = False
        remittance_validate_result['message'] = '本次放款总额必须不大于合同本金总计且不大于授信额度'
        return remittance_validate_result

    remittance_validate_result['status'] = True
    remittance_validate_result['message'] = ''

    return remittance_validate_result


def calc_interval_of_date_time(time_unicode):
    try:
        time_str = time_unicode.encode('utf-8')
        end_str = time.strftime('%Y-%m-%d', time.localtime())
        start = datetime.datetime.strptime(time_str, '%Y-%m-%d 00:00:00')
        end = datetime.datetime.strptime(end_str, '%Y-%m-%d')
        print("end_time is %s " % end)
        return (end - start).days
    except Exception, e:
        print("计算时间发生异常，异常信息是%s" % e)
        return 0

def get_rpc_remark(rpc_remittance_obj):
    rpc_result = json.loads(rpc_remittance_obj.rpc_response.rpc_result)
    remark = ''
    print 'rpc_result:', rpc_result
    if rpc_result.has_key('paidNoticInfos'):
        paidNoticInfos = rpc_result['paidNoticInfos']
        paidNoticInfo = paidNoticInfos[0]
        if paidNoticInfo.has_key('paidDetails'):
            paidDetails = paidNoticInfo['paidDetails']
            paidDetail = paidDetails[0]
            if paidDetail.has_key('result'):
                remark = paidDetail['result']

    print "remark:{}", remark
    return remark

def run_script(request_data):
    application_data = extract_application_data(request_data)
    print("获取到的application_data的内容是%s" % application_data)

    schedule_plan_uuid = request_data['schedulePlanUuid']
    remittance_service = RemittanceService()
    asset_service = AssetService()

    # 获取合同信息
    contract = asset_service.get_contract(application_data['merchantContractNo'])
    print("获取合同信息是%s" % contract)

    remittance_validate_result = remittance_validate_data(application_data)

    print("放款字段校验结束,结果是============>%s" % remittance_validate_result)
    if not remittance_validate_result['status']:
        remittance_service.update_remittance_status(application_data['remittanceApplicationUuid'], 3,
                                                    remittance_validate_result['message'])
        # 合同状态更新为放款失败
        asset_service.update_contract_status(contract['contractUuid'], 8, contract['vedaCreditNo'],
                                             contract['principalSum'], None, remittance_validate_result['message'])
        return

    # 放款校验成功后将状态置为处理中
    remittance_service.update_remittance_status(application_data['remittanceApplicationUuid'], 1,
                                                remittance_validate_result['message'])
    # 放款指令同步贷后
    call_postloan_data = extract_call_postloan_data(application_data)

    rpc_remittance_obj = RemittancePostloanCall("YOULIWANG_REMITTANCE_POSTLOAN", schedule_plan_uuid,
                                                call_postloan_data)
    yield rpc_remittance_obj.call()

    print("调用贷后的反馈结果是%s" % rpc_remittance_obj.response().rpc_result)

    remark = get_rpc_remark(rpc_remittance_obj).encode("utf-8")

    if rpc_remittance_obj.response().rpc_business_state == RESPONSE_BUSINESS_OK:
        print("remittance business is ok ---------------------")
        remittance_service.update_remittance_status(application_data['remittanceApplicationUuid'], 2, None)

        asset_service.update_contract_date(contract['merchantContractNo'])
        # 合同状态变更为放款成功
        asset_service.update_contract_status(contract['contractUuid'], 7, contract['vedaCreditNo'],
                                             contract['principalSum'], None, '放款成功')

    if rpc_remittance_obj.response().rpc_business_state == RESPONSE_BUSINESS_FAILED:
        print("remittance business is failed ---------------------")
        # 更新放款状态失败
        remittance_service.update_remittance_status(application_data['remittanceApplicationUuid'], 3, remark)
        # 合同状态更新为放款失败
        asset_service.update_contract_status(contract['contractUuid'], 8, contract['vedaCreditNo'],
                                             contract['principalSum'], None, remark)


def create_engin(request_data):
    runtime = run_script(request_data)
    engine = AsyncEngine(runtime)
    result = engine.one_step()
    if "end" != result:
        return engine
    else:
        return result
